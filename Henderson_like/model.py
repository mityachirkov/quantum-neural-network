import jax.numpy as jnp
from qhollab import simulate
from jax.scipy.special import logsumexp
from jax import value_and_grad, vmap
from quantum import *
from jax.example_libraries.optimizers import adam, adamax
from jax import jit



def relu(x):
  return jnp.maximum(0, x)

def one_hot(x, k, dtype=jnp.float32):
    """Create a one-hot encoding of x of size k."""
    return jnp.array(x[:, None] == jnp.arange(k), dtype)
@jit
def accuracy(params, images, targets, theta=None, conv=None):
    target_class = jnp.argmax(targets, axis=1)
    pred = predict(params, images, theta, conv)
    predicted_class = jnp.argmax(pred, axis=1)
#     print(f"{predicted_class=} {pred=}")
#     print(predicted_class.shape) 
    return jnp.mean(predicted_class == target_class)
@jit
def loss(params, images, targets, theta=None, conv = None):
    preds = predict(params, images, theta, conv)
    return -jnp.mean(preds * targets)

@jit
def dloss(params,x,y,theta, conv):
    dloss = value_and_grad(loss)(params,x,y,theta, conv)
    return dloss

def update(opt_state, x, y, step, optimizer, theta=None, conv=None):
    _, opt_update, get_params = optimizer
    params = get_params(opt_state)
    loss_res, grads = dloss(params,x,y,theta, conv)
    # if step%50==0:
    #     print('grad0', grads[0])
        # print('grad1', grads[1])
    opt_state = opt_update(step, grads, opt_state)  
    return loss_res, opt_state

class Conv:
    def __init__(self, kernel_num, kernel_size, kernel):
        """
        Constructor takes as input the number of kernels and their size. 
        """
        self.kernel_num = kernel_num
        self.kernel_size = kernel_size
        # Generate random filters of shape (kernel_num, kernel_size, kernel_size). Divide by kernel_size^2 for weight normalization
        self.num_conv = kernel.shape[0]
        self.kernels = kernel / (kernel_size**2)
        # self.kernels = np.zeros((self.kernel_size, self.kernel_size))
        # self.kernels[0][0] = 1
        # self.kernels = fft.fftshift(self.kernels)
        
        self.Fourier_kernel = jnp.fft.fft2(self.kernels)

    def image_prep(self, image):
        """
        Fourier-tranform of image and kernels 
        """
        Fourier_image = jnp.fft.fft2(image)
        return Fourier_image
    
    def forward(self, image, num):
        """
        Perform forward propagation for the convolutional layer.
        """
        image_f = self.image_prep(image)
        output_f = image_f*self.Fourier_kernel[:][:][num]#[0]
        output = jnp.fft.ifft2(output_f)
        #output = fft.ifftshift(output)
        output = jnp.array(output, float)
        return output
    
    def max_pooling(self, image):
        # Total number of pools
        num_pools = self.forward(image).shape[0]
        tgt_shape = (int(np.sqrt(num_pools)), int(np.sqrt(num_pools)))
        pooled = []
        
        for pool in self.forward(image):
            # Append the max value only
            pooled.append(np.max(self.forward(image)))
            
        # Reshape to target shape
        return np.array(pooled).reshape(tgt_shape)
    
@jit
def predict(params, image, theta = None, conv = None):
  # per-example predictions  
    if theta != None:
        image = image.reshape(len(image), 2,2,2,2,2,2)
        image = SV(image)
        for i in range(theta.shape[0]):
            circ = Сircuit_9(params[0][i], image)
            circ = Pooling_q(circ)
            circ *= Сircuit_9_small(params[1][i])
        #print(image.shape, circ)
        image = simulate(circ, image).data
        image= image.reshape((-1, 8, 8))

    if conv != None:
        
        image= image.reshape((-1,image.shape[1],image.shape[2]))
        for i in range(conv.num_conv):
            
            temp = conv.forward(image, i)
            image = temp

    activations = image.reshape(-1, image.shape[1]*image.shape[2])

    for w, b in params[2:-1]:
        outputs = jnp.dot(activations, w.T) + b[None]
        activations = relu(outputs)
    final_w, final_b = params[-1]
    logits = jnp.dot(activations, final_w.T) + final_b[None]
    return logits - logsumexp(logits, axis = 1)[:,None]


# import jax.numpy as jnp
# from qhollab import simulate
# from jax.scipy.special import logsumexp
# from jax import value_and_grad, vmap
# from quantum import *
# from jax.example_libraries.optimizers import adam, adamax
# from numba import jit


# def relu(x):
#   return jnp.maximum(0, x)

# def one_hot(x, k, dtype=jnp.float32):
#     """Create a one-hot encoding of x of size k."""
#     return jnp.array(x[:, None] == jnp.arange(k), dtype)

# def accuracy(params, images, targets, theta=None, conv=None):
#     target_class = jnp.argmax(targets, axis=1)
#     pred = predict(params, images, theta, conv)
#     predicted_class = jnp.argmax(pred, axis=1)
# #     print(f"{predicted_class=} {pred=}")
# #     print(predicted_class.shape) 
#     return jnp.mean(predicted_class == target_class)
# @jit
# def loss(params, images, targets, theta=None, conv = None):
#     preds = predict(params, images, theta, conv)
#     return -jnp.mean(preds * targets)
# @jit
# def update(opt_state, x, y, step, optimizer, theta=None, conv=None):
#     _, opt_update, get_params = optimizer
#     params = get_params(opt_state)
#     loss_res, grads = value_and_grad(loss)(params,x,y,theta, conv)
#     print(f'{grads[2]}')
#     print(f'grad1 = {grads[1]}')
#     print(f'grad0 =  {grads[0]}')
#     print(f'grad3 = {grads[3]}')
#     print(np.array(params).shape, 'params')
#     opt_state = opt_update(step, grads, opt_state)
#     return loss_res, opt_state

# class Conv:
#     def __init__(self, kernel_num, kernel_size, kernel):
#         """
#         Constructor takes as input the number of kernels and their size. 
#         """
#         self.kernel_num = kernel_num
#         self.kernel_size = kernel_size
#         # Generate random filters of shape (kernel_num, kernel_size, kernel_size). Divide by kernel_size^2 for weight normalization
#         self.kernels = kernel / (kernel_size**2)
#         # self.kernels = np.zeros((self.kernel_size, self.kernel_size))
#         # self.kernels[0][0] = 1
#         # self.kernels = fft.fftshift(self.kernels)
        
#         self.Fourier_kernel = jnp.fft.fft2(self.kernels)

#     def image_prep(self, image):
#         """
#         Fourier-tranform of image and kernels 
#         """
#         Fourier_image = jnp.fft.fft2(image)
#         return Fourier_image
    
#     def forward(self, image):
#         """
#         Perform forward propagation for the convolutional layer.
#         """
#         image_f = self.image_prep(image)
#         output_f = image_f*self.Fourier_kernel[:][:]#[0]
#         output = jnp.fft.ifft2(output_f)
#         #output = fft.ifftshift(output)
#         output = jnp.array(output, float)
#         return output
    
#     def pool2d(self, A, pool_size, stride, padding=0, pool_mode='max'):
#         '''
#         2D Pooling

#         Parameters:
#         A: input 2D array
#         kernel_size: int, the size of the window over which we take pool
#         stride: int, the stride of the window
#         padding: int, implicit zero paddings on both sides of the input
#         pool_mode: string, 'max' or 'avg'
#         '''
#         # Padding
#         A = np.pad(A, padding, mode='constant')

#         # Window view of A
#         output_shape = ((A.shape[0] - pool_size) // stride + 1, (A.shape[1] - pool_size) // stride + 1)

#         shape_w = (output_shape[0], output_shape[1], pool_size, pool_size)
#         strides_w = (stride*A.strides[0], stride*A.strides[1], A.strides[0], A.strides[1])

#         A_w = as_strided(A, shape_w, strides_w)

#         # Return the result of pooling
#         if pool_mode == 'max':
#             return A_w.max(axis=(2, 3))
#         elif pool_mode == 'avg':
#             return A_w.mean(axis=(2, 3))

# #@jit
# def predict(params, image, theta = None, conv = None):
#   # per-example predictions  
#     print('h')
#     if theta != None:
#         image = image.reshape(len(image), 2,2,2,2,2,2)
#         for i in range(theta.shape[0]):
#             image = SV(image)
#             image = simulate(Сircuit_9(theta[i]), image).data#measure_all
#         image= image.reshape((-1, 8, 8))

#     if conv != None:
        
#         image= image.reshape((-1,image.shape[1],image.shape[2]))
#         for conv_layer in conv:
            
#             temp = conv_layer.forward(image)
#             #print(temp.shape)
#             image = np.array([conv_layer.pool2d(temp[i], pool_size=2,stride = 2) for i in range(temp.shape[0])])
#             #print(image.shape)
#             #image = temp
#     print('e')
#     activations = image.reshape(-1, image.shape[1]*image.shape[2])

#     for w, b in params[:-1]:
#         print('w,b')
#         outputs = jnp.dot(activations, w.T) + b[None]
#         activations = relu(outputs)
#     final_w, final_b = params[-1]
#     logits = jnp.dot(activations, final_w.T) + final_b[None]
#     return logits - logsumexp(logits, axis = 1)[:,None]

