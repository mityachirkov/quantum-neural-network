import numpy as np
import jax.numpy as jnp
from qhollab import*

def Сircuit_9(theta: list, sv, measure = False):
    theta = jnp.pi*10*theta
    # circ = Qft()
    circ = H[0]*H[0] 
    len_sv = len(sv.data_shape)
    #print(len_sv)
    for i in range(len_sv-1):
        circ *= H[i]
    # for i in range(len_sv):
    #     circ *= SWAP[i, i+1]
    for i in range(len_sv-1):
        circ *= RotationX(theta[i])[i]
    # layer = H[0]*H[1]*H[2]*H[3]*H[4]*H[5]*SWAP[5,4]*SWAP[3,4]*Swap()[2,3]*SWAP[2,1]*SWAP[0,1]*RotationX(theta[0])[0]*RotationX(theta[1])[1]*RotationX(theta[2])[2]*RotationX(theta[3])[3]*RotationX(theta[4])[4]*RotationX(theta[5])[5]
    # for i in range(1):
    #     circ *= layer
    if measure == True:
        for i in range(len_sv-1):
            circ *= Measurement(i)
    #print(circ)
    return circ

def Сircuit_9_small(theta: list):
    theta = jnp.pi*10*theta
    # circ = Qft()
    circ = H[0]*H[0]
    layer = H[0]*H[1]*H[2]*H[3]*SWAP[2,1]*SWAP[2,3]*SWAP[0,1]*RotationX(theta[0])[0]*RotationX(theta[1])[1]*RotationX(theta[2])[2]*RotationX(theta[3])[3]#*RotationX(theta[4])[4]
    for i in range(1):
        circ *= layer
    
    circ *= Measurement(0)*Measurement(1)*Measurement(2)*Measurement(3)*Measurement(4)*Measurement(5)#*Measurement(6)*Measurement(7)#*Measurement(8)*Measurement(9)
    
    return circ

def Circuit_14(theta):
    circ = Qft()
    layer = RotationY(theta[0])[0]*RotationY(theta[1])[1]*RotationY(theta[2])[2]*RotationY(theta[3])[3]*RotationY(theta[4])[4]*RotationY(theta[5])[5]*CRX(5, 0, theta[6])*CRX(4, 5, theta[7])*CRX(3, 4, theta[8])*CRX(2, 3, theta[9])*CRX(1, 2, theta[10])*CRX(0, 1, theta[11])*RotationY(theta[12])[0]*RotationY(theta[13])[1]*RotationY(theta[14])[2]*RotationY(theta[15])[3]*RotationY(theta[16])[4]*RotationY(theta[17])[5]*CRX(5, 4, theta[18])*CRX(0, 5, theta[19])*CRX(1, 0, theta[20])*CRX(2, 1, theta[21])*CRX(3, 2, theta[22])*CRX(4, 3, theta[23])
    for i in range(1):
        circ *= layer
    circ *= Qft()*Measurement(0)*Measurement(1)*Measurement(2)*Measurement(3)*Measurement(4)*Measurement(5)
    return circ

def Pooling_q(circ):
    '''
    For now we estimated only 2-by-2 kernel
    '''
    scheme_len = circ.estimated_len
    for i in range(scheme_len-4):
        circ *= CNOT(i+1, i)*CNOT(i+2, i)*CNOT(i+3, i)*CNOT(i+4, i)#*CNOT(i+5, i)
    return circ

def Param_Shift(circ, theta, pic):
    #grad = (simulate(circ(theta+np.pi/2*np.ones(theta.shape)), pic).data-simulate(circ(theta+np.pi/2*np.ones(theta.shape)), pic).data)/2
    grad = circ(theta+np.pi/2*np.ones(theta.shape))(pic)-circ(theta+np.pi/2*np.ones(theta.shape))(pic)/2
    return grad