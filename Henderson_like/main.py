#!/usr/bin/env python3

import time
import matplotlib.pyplot as plt
from model import *
from data_loader import *
from layers_init import *
from jax.example_libraries.optimizers import adam, adamax

layer_sizes = [16, 10]

params = init_network_params(layer_sizes, random.PRNGKey(0))
step_size = 0.1
num_epochs = 10
num_it = 1
batch_size = 128
n_targets = 10
kernel_size = 8
kernel_numb = 1
num_quantum = 1
num_conv = 1
kernel = np.random.randn(kernel_numb, kernel_size, kernel_size)
kernel2 = np.random.randn(kernel_numb, int(kernel_size/2), int(kernel_size/2))

theta = jnp.asarray(0.1*np.random.random((num_quantum, 6)))
theta_small = jnp.asarray(0.1*np.random.random((num_quantum, 5)))
theta14 = jnp.asarray(90*np.random.random(24), dtype = int)
print(len(theta))
print(len(params))
params.insert(0, theta_small)
params.insert(0, theta)

loss_history = []
loss_history_train = []
loss_history_test = []
train_accuracy = []
test_accuracy = []
step_size = 0
opt_init, opt_state, get_params = adam(1e-3, b1 = 0.9, b2 = 0.999)
opt_state = opt_init(params)

training_generator, train_images, train_labels, test_images, test_labels = data(batch_size, n_targets)
f = open('result8_wo_qft.txt', 'w')
for epoch in range(num_epochs):
    start_time = time.time()
    for x, y in training_generator:
        for it in range(num_it):
            yy = one_hot(y, n_targets)
            
            loss_res, opt_state = update(opt_state, x, yy, step_size, optimizer = adam(1e-3, b1 = 0.9, b2 = 0.999), theta = theta, conv=None)
            loss_history.append(float(loss_res))
            step_size += batch_size

    epoch_time = time.time() - start_time
    params = get_params(opt_state)
    #loss_train = loss(params, train_images, train_labels, theta = None, conv=my_conv)
    loss_test = loss(params, test_images, test_labels, theta = theta, conv=None)
    #loss_history_train.append(float(loss_train))
    loss_history_test.append(float(loss_test))
    train_acc = accuracy(params, train_images, train_labels, theta = theta, conv=None)
    test_acc = accuracy(params, test_images, test_labels, theta = theta, conv=None)
    train_accuracy.append(train_acc)
    test_accuracy.append(test_acc)
    f.write(f"Epoch {epoch} in {epoch_time} sec" + '\n')
    f.write(f"Training set accuracy {format(train_acc)} loss {loss_history[-1]}" + '\n')
    f.write("Test set accuracy {}".format(test_acc))
    print("Epoch {} in {:0.2f} sec".format(epoch, epoch_time))
    print(f"Training set accuracy {format(train_acc)} loss {loss_history[-1]}")
    print("Test set accuracy {}".format(test_acc))
    # print(f'theta = {90*params[0]}')
print(max(test_accuracy))
f.close()
fig, ax = plt.subplots(1, 1)
__=ax.plot((loss_history_test))
xlabel = ax.set_xlabel(r'${\rm step\ number}$')
ylabel = ax.set_ylabel(r'${\rm loss}$')
title = ax.set_title(r'${\rm test\ history, quantum}$')
fig.savefig('test_loss_16.pdf')