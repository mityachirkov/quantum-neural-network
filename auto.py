

pixels = 16
qlayers = 3
features = 2

def qparams(params, pixels, qlayers, features):
    n = int(np.log2(pixels)-1)
    shape = np.multiply(np.ones(2*n, dtype=int), 2, dtype=int)
    matrix_el = 2**(2*features)-1
    p1 = np.array(np.pi*np.random.random((matrix_el, *shape)))
    params.append(p1)
    for i in range(1, qlayers):
        for _ in range(3):
            p2 = np.array(np.pi*np.random.random((matrix_el, *shape[:-(2*i)])))
            params.append(p2)

def arch(params,pixels, qlayers, features):
    '''
    Архитектура с использыванием одной пары измерений в каждом слое
    '''
    n = int(np.log(pixels))+features
    qubits = 2*n-features
    indexes = np.arange(qubits)
    temp_ind = np.arange(qubits)
    circ = Qft()[*indexes[n:]]*Qft()[*indexes[:n]]*Diag_matrix(params[0])*iQft()[*indexes[n:]]*iQft()[*indexes[:n]]
    for i in range(1,qlayers):
        temp_ind = np.delete(temp_ind, (n+features, -1))
        circ *= Qft()[*indexes[features:n-i]]*Qft()[*temp_ind[:n]]
        circ *= CDiag_matrix_0(indexes[-i], temp_ind, params[3*(i-1)+1])*CDiag_matrix_1(indexes[-i], temp_ind, params[3*(i-1)+2])*CDiag_matrix_0(indexes[-i], temp_ind, params[3*(i-1)+3])
        circ *= iQft()[*indexes[features:n-i]]*iQft()[*temp_ind[:n]]
