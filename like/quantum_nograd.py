import jax.numpy as jnp
# import jax.numpy as jnp
from qhollab import*
from jax.scipy.linalg import block_diag
import itertools

def Сircuit_(theta, theta2, sv, measure = True, qsize = 2):
    '''
    measure -- initially True; if False turn off measurement of all qubits
    qsize -- amount of qubits for on of coordinate of rectangular picture
    '''
    #???разные икс и игрек брать с разными весами??? в кодировании frqi
    len_sv = len(sv.data_shape)
    
    if qsize == 2:
        # theta = theta.reshape(2,2,2,2,2)
        index_f = [1,2,3,4]
        # circ = Qft()[1 ,2]*Qft()[3,4]*Block_diag_matrix(theta[::-1])*iQft()[1,2]*iQft()[3,4]#*X[0]*Qft()[1,2]*Qft()[3,4]*Block_diag_matrix(theta[::-1])*iQft()[1,2]*iQft()[3,4]
        #два слоя
        circ = Qft()[1 ,2]*Qft()[3,4]*Block_diag_matrix(theta[::-1])*iQft()[1,2]*iQft()[3,4]#*Qft()[1 ,2]*Qft()[3]*CBlock_diag_matrix_1(4, index_f[:-1], theta2)*iQft()[1,2]*iQft()[3]
        # circ *= Block_pairwise_mult_circ(index_f, theta, 1)
        # circ *= iQft()[1,2]*iQft()[3,4]
        # circ = Qft()[0]*Qft()[1 ,2]*Qft()[3,4]*Diag_matrix(theta[::-1])*iQft()[0]*iQft()[1,2]*iQft()[3,4]
        # circ = Qft()[0,1]*Qft()[2,3]*Diag_matrix(theta)*iQft()[0,1]*iQft()[2,3]#iQft 
        # circ = Qft()[0,1]*Qft()[2,3]*CMult(0, [1,2,3], theta[:3])*CMult(1, [0,2,3], theta[3:6])*CMult(2, [0,2,3], theta[6:9])*CMult(3, [0,1,2], theta[3])*iQft()[0,1]*iQft()[2,3]
        # circ = Qft()[0,1]*Qft()[2,3]*CMult(0, [1,2,3], theta[0,...])*CMult(1, [0,2,3], theta[:,0,...])*CMult(2, [0,1,3], theta[...,0,:])*CMult(3, [0,1,2], theta[...,0])*iQft()[0,1]*iQft()[2,3]
    if qsize == 3:
        # theta = theta.reshape(2,2,2,2,2,2,2)
        # theta2 = theta2.reshape(2,2,2,2,2,2,2)
        index_f = [0,1,2,3,4,5,6]
        
        circ = Qft()[1 ,2,3]*Qft()[4,5,6]
        circ *= Block_pairwise_mult_circ(circ, index_f, theta,3)
        circ *= iQft()[1 ,2,3]*iQft()[4,5,6]
        # circ = Qft()[1 ,2,3]*Qft()[4,5,6]*Block_diag_matrix(theta[::-1])*iQft()[1 ,2,3]*iQft()[4,5,6]#*X[0]*Qft()[0]*Qft()[1 ,2,3]*Qft()[4,5,6]*Block_diag_matrix(theta2[::-1])*iQft()[0]*iQft()[1 ,2,3]*iQft()[4,5,6]
        # circ = Qft()[0,1,2]*Qft()[3,4,5]*Diag_matrix(theta)*iQft()[0,1,2]*iQft()[3,4,5] #iQft 
    if qsize == 4:
        # theta = theta.reshape(2,2,2,2,2,2,2,2,2)
        index_f = [0,1,2,3,4,5,6,7,8]
        circ = Qft()[1 ,2,3,4]*Qft()[5,6,7,8]*Block_diag_matrix(theta[::-1])*iQft()[1 ,2,3,4]*iQft()[5,6,7,8]*Qft()[1 ,2,3,4]*Qft()[5,6,7]*CBlock_diag_matrix_1(index_f[-1], index_f[:-1], theta2)*iQft()[1 ,2,3,4]*iQft()[5,6,7]
        # circ = Qft()[0,1,2,3]*Qft()[4, 5,6,7]*Diag_matrix(theta)*iQft()[0,1,2,3]*iQft()[4,5,6,7] #базовое домножение на диагональную матрицу для exp
        # circ = Qft()[1 ,2,3,4]*Qft()[5,6,7,8]
        # circ *= Block_pairwise_mult_circ(circ, index_f, theta,2)
        # circ *= iQft()[1 ,2,3,4]*iQft()[5,6,7,8]
        # circ = Qft()[1 ,2,3,4]*Qft()[5,6,7,8]*Block_diag_matrix(theta[::-1])*iQft()[1 ,2,3,4]*iQft()[5,6,7,8]#*X[0]*Qft()[1 ,2,3,4]*Qft()[5,6,7,8]*Block_diag_matrix(theta[::-1])*iQft()[1 ,2,3,4]*iQft()[5,6,7,8]
        # домножение на блочно-диагональную матрицу, базово для frqi
        # circ = Qft()[0,1,2,3]*Qft()[4, 5,6,7]*CMult(0, [1,2,3,4,5,6,7], theta[0,...])*CMult(1, [0,2,3,4,5,6,7], theta[:,0,...])*CMult(2, [0,1,3,4,5,6,7], theta[:,:,0,...])*CMult(3, [0,1,2,4,5,6,7], theta[:,:,:,0,...])*CMult(4, [0,1,2,3,5,6,7], theta[:,:,:,:,0,...])*CMult(5, [0,1,2,3,4,6,7], theta[...,0,:,:])*CMult(6, [0,1,2,3,4,5,7], theta[...,0,:])*CMult(7, [0,1,2,3,4,5,6], theta[...,0])*iQft()[0,1,2,3]*iQft()[4,5,6,7]
        # circ = Qft()[0,1,2,3]*Qft()[4, 5,6,7]*CMult(0, [1,2,3,4,5,6,7], theta[1,...])*CMult(1, [0,2,3,4,5,6,7], theta[:,1,...])*CMult(2, [0,1,3,4,5,6,7], theta[:,:,1,...])*CMult(3, [0,1,2,4,5,6,7], theta[:,:,:,1,...])*CMult(4, [0,1,2,3,5,6,7], theta[:,:,:,:,1,...])*CMult(5, [0,1,2,3,4,6,7], theta[...,1,:,:])*CMult(6, [0,1,2,3,4,5,7], theta[...,1,:])*CMult(7, [0,1,2,3,4,5,6], theta[...,1])*iQft()[0,1,2,3]*iQft()[4,5,6,7]
    if qsize ==5:
        index_f = [0,1,2,3,4,5,6,7,8,9,10]
        circ = Qft()[1,2,3,4,5]*Qft()[6,7,8,9,10]*Block_diag_matrix(theta[::-1])*iQft()[1,2,3,4,5]*iQft()[6,7,8,9,10]
    if measure == True:
        for i in range(len_sv-1):
            circ *= Measurement(i)
        # print(f'{i=}')

    return circ

class Diag_matrix(Circuit):
    def __init__(self, theta):
        super().__init__(name='MAT') 
        self.theta = theta.reshape(-1)
    
    def apply(self, sv):
        assert isinstance(sv, StateVector)
        self.init(sv.ss)
        matrix = jnp.exp(-1j*self.theta)
        # result = sv._data.reshape(-1,16,16) * matrix
        result = sv._data.reshape(-1,self.ss.dim) * matrix
        return SV(
            data=result.reshape(sv._data.shape),
            ndim=sv._ndim
        )
    def inverse(self):
        return self

class iQft(Circuit):
    def __init__(self):
        super().__init__(name='iQFT') 
        
    def apply(self, sv):
        assert isinstance(sv, StateVector)
        self.init(sv.ss)
        if isinstance(sv, SV):
            result = jnp.fft.ifft(sv._data.reshape((-1,sv.ss.dim)), norm="ortho")
            return SV(
                data=result.reshape(sv._data.shape),
                ndim=sv._ndim
            )
        raise NotImplementedError

def CDiag_matrix_0(c, i,theta): return Condition(Diag_matrix(theta[::-1]), cond={c:0}, indices=[*i])

def CDiag_matrix_1(c, i,theta): return Condition(Diag_matrix(theta[::-1]), cond={c:1}, indices=[*i])

class Block_diag_matrix(Circuit):
    def __init__(self, theta, vector_n = None):
        super().__init__(name='BMAT') 
        self.theta = theta.reshape(-1,3)
        self.vector_n = vector_n
    
    def apply(self, sv):
        assert isinstance(sv, StateVector)
        self.init(sv.ss)
        # print(f'{sv._data.shape=} {sv.ss=}')
        u = jnp.array([U3(t) for t in self.theta])
        if self.vector_n == None:
            u = u.reshape(*sv._data.shape[1:],2)
        else:
            shap = 2*jnp.ones(self.vector_n, dtype= int)
            u = u.reshape(*shap,2,2)
            # u = u.reshape(*sv._data.shape[1:],2)
        # print(f'{u.shape=} {sv._data.shape=}')
        result = jnp.stack([u[0,0,...]*sv._data[:,0,...]+u[0,1,...]*sv._data[:,1,...],u[1, 0,...]*sv._data[:,0,...]+u[1,1,...]*sv._data[:,1,...]], axis = 1)
        return SV(
            data=result,
            ndim=sv._ndim)
    
    def inverse(self):
        return self

def CBlock_diag_matrix_0(c, i,theta): return Condition(Block_diag_matrix(theta[::-1]), cond={c:0}, indices=[*i])

def CBlock_diag_matrix_1(c, i,theta): return Condition(Block_diag_matrix(theta[::-1]), cond={c:1}, indices=[*i])

def CMult(c, i, theta): return Condition(Diag_matrix(theta), cond={c:0}, indices=[*i])

def U3(ang): 
    t,p,l = ang
    return [    [jnp.cos(t/2), -jnp.exp(1j*l)*jnp.sin(t/2)],
                [jnp.exp(1j*p)*jnp.sin(t/2), jnp.exp(1j*(p+l))*jnp.cos(t/2)]    ]


def Pairwise_mult_circ(circ, index, theta):
    #circ = Qft()[1 ,2]*Qft()[3,4]#Qft()[0,1,2,3]*Qft()[4,5,6,7] #тут было Qft()[1 ,2]*Qft()[3,4] и работало лучше, почему?
    pair_order_list = itertools.combinations(index,2)
    q = list(pair_order_list)
    theta = theta.reshape(-1,1)
    for n, i in enumerate(q):
        circ*=Diag_matrix(theta[n])[i]
    # circ *= iQft()[1 ,2]*iQft()[3,4]
    return circ 

def Block_pairwise_mult_circ(circ, index, theta, pair_n):
    # circ = Qft()[1 ,2]*Qft()[3,4]#Qft()[0,1,2,3]*Qft()[4,5,6,7] #тут было Qft()[1 ,2]*Qft()[3,4] и работало лучше, почему?
    pair_order_list = itertools.combinations(index,pair_n)
    q = list(pair_order_list)
    theta = theta.reshape(-1,3*(2**pair_n))
    for n, i in enumerate(q):
        circ*=Block_diag_matrix(theta[n],pair_n)[i]
    # circ *= iQft()[1 ,2]*iQft()[3,4]
    # print(circ)
    return circ 

def Pooling_q(circ):
    '''
    For now we estimated only 2-by-2 kernel
    '''
    scheme_len = circ.estimated_len
    for i in range(scheme_len-4):
        circ *= CNOT(i+1, i)*CNOT(i+2, i)*CNOT(i+3, i)*CNOT(i+4, i)#*CNOT(i+5, i)
    return circ

def Param_Shift(circ, theta, pic):
    #grad = (simulate(circ(theta+np.pi/2*np.ones(theta.shape)), pic).data-simulate(circ(theta+np.pi/2*np.ones(theta.shape)), pic).data)/2
    grad = circ(theta+np.pi/2*np.ones(theta.shape))(pic)-circ(theta+np.pi/2*np.ones(theta.shape))(pic)/2
    return grad

def Toffoli(c1, c2, i): return Condition(X, cond = {c1:1, c2:1}, indicies = [i])