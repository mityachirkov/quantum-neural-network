#!/usr/bin/env python3

import time
import matplotlib.pyplot as plt
from model import *
from data_loader import *
from layers_init import *
from jax.example_libraries.optimizers import adam, adamax
pixels = 4
n_targets = 2
layer_sizes = [pixels**2, n_targets]

params = init_network_params(layer_sizes, random.PRNGKey(0))
step_size = 0.1
num_epochs = 5
num_it = 1
batch_size = 128
THRESHOLD = 0.5

binaries = [[0,9], [1,7], [2,9], [4,6], [6,7], [8,9]]
num_quantum = 1#len(binaries)

theta = jnp.asarray(0.1*2*np.pi*np.random.random((num_quantum, pixels**2)))
coef = jnp.asarray(np.random.random((num_quantum, pixels**2)))
print(theta) 
params.insert(0, coef)
params.insert(0, theta)
# params.insert(0, my_conv)

for f, s in binaries:
    print(f'Binary classification of {f} and {s}')
    loss_history = []
    loss_history_train = []
    loss_history_test = []
    train_accuracy = []
    test_accuracy = []
    step_size = 0
    opt_init, opt_state, get_params = adam(1e-3, b1 = 0.9, b2 = 0.999)
    opt_state = opt_init(params)
    i = 0
    
    training_generator, train_images, train_labels, test_images, test_labels = data(batch_size, n_targets, pixels)
    train_images, train_labels = filter_(train_images, train_labels, f, s)
    test_images, test_labels = filter_(test_images, test_labels, f, s)
    train_images = train_images/255.
    test_images = test_images/255.
    train_images = np.exp(1j*np.pi*train_images)
    test_images = np.exp(1j*np.pi*test_images)
    train_labels = one_hot(train_labels, n_targets)
    test_labels = one_hot(test_labels, n_targets)
    # train_images = np.array(train_images > THRESHOLD, dtype=np.int).reshape(-1, train_images.shape[1]*train_images.shape[2])
    # test_images = np.array(test_images > THRESHOLD, dtype=np.int).reshape(-1, test_images.shape[1]*test_images.shape[2])
    print(test_images[0])
    f = open('result'+str(pixels)+'_w_qft.txt', 'w')
    for epoch in range(num_epochs):
        start_time = time.time()
        for x, y in training_generator:
            x, y = filter_(x, y, f, s)
            for it in range(num_it):
                yy = one_hot(y, n_targets)
                
                loss_res, opt_state = update(opt_state, train_images, train_labels, step_size, i, optimizer = adam(1e-3, b1 = 0.9, b2 = 0.999))
                loss_history.append(float(loss_res))
                # if loss_res < 1e-3:
                #     print(f'{loss_res =}, {yy = }')
                step_size += batch_size

        epoch_time = time.time() - start_time
        params = get_params(opt_state)
        print(params[0])
        #loss_train = loss(params, train_images, train_labels, theta = None, conv=my_conv)
        loss_test = loss(params, test_images, test_labels, i)
        #loss_history_train.append(float(loss_train))
        print('labels',train_labels)
        print('loss test',loss_test)
        print('loss history',loss_history)
        loss_history_test.append(float(loss_test))
        train_acc = accuracy(params, train_images, train_labels, i)
        test_acc = accuracy(params, test_images, test_labels, i)
        train_accuracy.append(train_acc)
        test_accuracy.append(test_acc)
        f.write(f"Epoch {epoch} in {epoch_time} sec" + '\n')
        f.write(f"Training set accuracy {format(train_acc)} loss {loss_history[-1]}" + '\n')
        f.write("Test set accuracy {}".format(test_acc))
        print("Epoch {} in {:0.2f} sec".format(epoch, epoch_time))
        print(f"Training set accuracy {format(train_acc)} loss {loss_history[-1]}")
        print("Test set accuracy {}".format(test_acc))
        # print(f'theta = {90*params[0]}')`
print(max(test_accuracy))
f.close()
fig, ax = plt.subplots(1, 1)
__=ax.plot((loss_history_test))
xlabel = ax.set_xlabel(r'${\rm step\ number}$')
ylabel = ax.set_ylabel(r'${\rm loss}$')
title = ax.set_title(r'${\rm test\ history, quantum}$')
fig.savefig('test_loss_16_matrix_multiplication.pdf')