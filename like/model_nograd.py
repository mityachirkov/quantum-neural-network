import jax.numpy as jnp
import numpy as np
import math
from qhollab import simulate
from quantum_nograd import *
from jax import jit
from sklearn.metrics import confusion_matrix
from functools import partial
from scipy.optimize import minimize
# from jax.scipy.optimize import minimize

def log_softmax(x):
    c = x.max()
    logsumexp = jnp.log(jnp.exp(x - c).sum())
    return x - c - logsumexp

def one_hot(x, k, dtype=jnp.float32):
    """Create a one-hot encoding of x of size k."""
    return jnp.array(x[:, None] == jnp.arange(k), dtype)
# @partial(jit, static_argnums=(3,4))
def accuracy(params, images, targets, pixels, encoding, shapes):
    target_class = jnp.argmax(targets, axis=1)
    pred = predict(params, images, pixels, encoding, shapes)
    predicted_class = jnp.argmax(pred, axis=1)
    conf = confusion_matrix(target_class, predicted_class)
    print(conf)
    return jnp.mean(predicted_class == target_class)

@partial(jit, static_argnums=(3,4,5))
def loss(params,x,targets,pixels,encoding, shapes):
    # print(f'{params.shape=} {x.shape=} {targets.shape=}')
    preds = predict(params, x, pixels, encoding, shapes)
    preds = log_softmax(preds)
    preds = preds.reshape(targets.shape)
    output = -jnp.mean(preds * targets)
    # print(output)
    return  output

def update(params, x, y, pixels, encoding, shapes):
    # params = get_params(opt_state)
    # print(f'before {params[0]=}')
    res = minimize(loss, params, args=(x,y,pixels,encoding, shapes), tol=1e-3, method='COBYLA', options={'maxiter':15, 'disp':False})# no_jax(params,x,y, pixels, encoding, shapes)
    loss_res, params = res.fun, res.x
    
    # print(f'after {params[0]=}')
    params = compress(params, shapes)
    # opt_state = opt_update(step, grads, opt_state)
    return loss_res, params
# @partial(jit, static_argnums=(3,4,5))
def no_jax(params,x,y,pixels,encoding, shapes):
    params = np.array(params)
    # print(params.dtype, params.shape)
    dloss = minimize(loss, params, args=(x,y,pixels,encoding, shapes), tol=1e-3, method='COBYLA', options={'maxiter':5, 'disp':False})
    # print(f'{params_A_shape=} {params_B_shape=} {params_C_shape=}')
    # print(f'{dloss.x=} {dloss.x.shape=}')    
    loss_ = jnp.asarray(dloss.fun)
    params_ = jnp.asarray(dloss.x)
    params = compress(params_, shapes)
    return loss_, params #dloss.x

def compress(params_, shapes):
    a1, a2 = 0, 0
    arams = []
    for it in shapes:
        a2 += math.prod([i for i in it])
        arams.append(params_[a1:a2].reshape(it))
        a1 = a2
    return arams
    
    
@partial(jit, static_argnums=(2,3,4))
def predict(params_, image, pixels, encoding, shapes):
    '''
    params -- array of parameters of network

    pixels -- some power of 2

    encoding -- 'FRQI' or 'Exp'
    '''
    
    params = compress(params_, shapes)
    if pixels == 32 and encoding == 0:
        image = image.reshape(-1, 2,2,2,2,2,2,2,2,2,2)
        qsize = 5
    if pixels == 32 and encoding == 1:
        image = image.reshape(-1, 2,2,2,2,2,2,2,2,2,2,2)
        qsize = 5

    if pixels == 16 and encoding == 0:
        image = image.reshape(-1, 2,2,2,2,2,2,2,2)
        qsize = 4
    if pixels == 16 and encoding == 1:
        image = image.reshape(-1, 2,2,2,2,2,2,2,2,2)
        qsize = 4
    
    if pixels == 8 and encoding == 0:
        image = image.reshape(-1, 2,2,2,2,2,2)
        qsize = 3
    if pixels == 8 and encoding == 1:
        image = image.reshape(-1, 2,2,2,2,2,2,2)
        qsize = 3
    
    if pixels == 4 and encoding == 0:
        image = image.reshape(-1, 2,2,2,2)
        qsize = 2
    if pixels == 4 and encoding == 1:
        image = image.reshape(-1, 2,2,2,2,2)
        qsize = 2
    
    # print(f'{image.shape=} \n ')
    image = SV(image)
    
    if encoding == 0:
        circ = Сircuit_(params[0], params[1], image, measure=True, qsize=qsize)
        image = simulate(circ, image).data
        activations = image.reshape(-1, pixels*pixels)/pixels**2
        for w, b in params[1:]:
            outputs = jnp.dot(activations, w.T) + b[None]
        # activations = relu(outputs)
    # print(outputs.shape)
    if encoding == 1:
        circ = Сircuit_(params[0], params[1],image, measure=True, qsize=qsize)
        image = simulate(circ, image).data.sum(axis=1)
    # print(f'{image[0,0]=}')
    # print(f'{image.shape=} \n ')
    # print(f'{image.shape=} \n ')

        activations = image.reshape(-1, pixels*pixels)/pixels**2
        for it in params[2:]:
            # print(it.shape, it)
            outputs = jnp.dot(it[...,:-1], activations.T).T + it[...,-1]
            activations = outputs
        # activations = relu(outputs)
    # print(outputs.shape)
    # logits = relu(outputs)
    output = log_softmax(outputs)
    # print(f'{activations[0ations, final_w.T) + final_b[None]
    
    # return logits - logsumexp(logits, axis = 1)[:,None]
    return output
