#!/usr/bin/env python3

import time
import jax
import matplotlib.pyplot as plt
import joblib
import os
from tqdm import tqdm
from model import *
from data_loader import *
from layers_init import *
from jax.example_libraries.optimizers import adam
import warnings
warnings.filterwarnings('ignore')
# jax.config.update('jax_platform_name', 'cpu')
pixels = 32
encoding = 1
step_size = 0.1
num_epochs = 1
num_it = 1
batch_size = 64 #для 32на32 уменьшить батч в 4 раза
nlayers = [2]#len(binaries)
nfeatures = [2]
n_targets = 10

for n_features in nfeatures:
    for qlayers in nlayers:
        path = 'cm_best'#f'additional_training_{n_features}f_{qlayers}l_{pixels}'#'2_layers_'+str(pixels)+'_2f'#'new_second_layer'#'two_features_on_chosen_qubits'#

        if os.path.exists(path):
            os.chdir(path)
        else:
            os.mkdir(path)
            os.chdir(path)

        acc_and_loss = open(f'acc_and_cm.npy', 'wb')
        # model = open('model', 'wb')
        'encoding: 0 -- exp, 1 -- frqi, 2 -- threshold'


        downloading_file = os.path.dirname(__file__)  +'/2l2f_32_pixels.sav'
#f'/{qlayers-1}l{n_features}f_finalized_model_{pixels}_pixels.sav'

        layer_sizes = [int(2**(2*np.log2(pixels)+n_features-2*(qlayers-1))), 2*pixels, n_targets]



        if downloading_file == None:
            
            params = init_network_params(layer_sizes, random.PRNGKey(0))
            # print(np.shape(params))
            if encoding == 0:
                theta = jnp.asarray(np.pi*np.random.random(pixels**2))
                params.insert(0, theta)
            if encoding == 1:
                qparams(params, pixels, qlayers, n_features)

        else:
            # params = init_network_params(layer_sizes, random.PRNGKey(0))
            params = joblib.load(downloading_file)
            # for it in params2[2:]:
            #     params.append(it)
            # qparams(params, pixels, qlayers, n_features, first_layer=qlayers-1)
            # loaded_model = joblib.load(downloading_file)
            # print(np.shape(loaded_model[3]))
            # params = init_network_params(layer_sizes, random.PRNGKey(0))
            # for i in range(4):
            #     params.insert(i, loaded_model[i])
            ###############################################################################################
            #для дообучения с 3м слоем
            # theta5 = jnp.asarray(np.pi*np.random.random((15,2,2)))
            # params.insert(4, theta5)
            # theta6 = jnp.asarray(np.pi*np.random.random((15,2,2)))
            # params.insert(5, theta6)
            # theta7 = jnp.asarray(np.pi*np.random.random((15,2,2)))
            # params.insert(6, theta7)
            ###############################################################################################


        loss_history = []
        loss_history_train = []
        loss_history_test = []
        train_accuracy = []
        test_accuracy = [] 
        cm = [] 
        step_size = 0
        opt_init, opt_update, get_params = adam(1e-3, b1 = 0.9, b2 = 0.999)
        opt_state = opt_init(params)
        i = 0
        training_generator, test_images, test_labels = data(batch_size, n_targets, pixels)

        if encoding == 0:
            # train_images = np.exp(1j*np.pi*train_images/255.)
            test_images = np.exp(1j*np.pi*test_images/255.)

        if encoding == 1:
            # train_images = np.array([np.sin(np.pi*train_images/255.), np.cos(np.pi*train_images/255.)])
            # train_images = np.swapaxes(train_images,0,1)
            
            test_images = np.array([np.sin(np.pi*test_images/255.), np.cos(np.pi*test_images/255.)],dtype=complex)
            test_images = np.swapaxes(test_images,0,1)

        if encoding == 2:
            THRESHOLD = 0.5
            # train_images = np.array(train_images > THRESHOLD, dtype=np.int).reshape(-1, train_images.shape[1]*train_images.shape[2])
            test_images = np.array(test_images > THRESHOLD, dtype=np.int).reshape(-1, test_images.shape[1]*test_images.shape[2])

        test_labels = one_hot(test_labels, n_targets)
        grad_history=[]

        for epoch in tqdm(range(num_epochs)):
            start_time = time.time()
            loss_history = []
            loss_history_t = []
            test_accuracy_=[]
            for x, y in training_generator:
                # x=test_images[:10]
                # y = test_labels[:10]
                # print(y[0])
                # print(x[0][6:-6,6:-6])
                if encoding == 1:
                    x = jnp.array([jnp.sin(np.pi*x/255.), jnp.cos(np.pi*x/255.)], dtype=complex)
                    x = np.swapaxes(x,0,1)
                if encoding == 0:
                    x = np.exp(1j*np.pi*x/255.)
                if encoding == 2:
                    x = np.array(x > THRESHOLD, dtype=np.int).reshape(-1, x.shape[1]*x.shape[2])
                yy = one_hot(y, n_targets)
                loss_res, opt_state, gradient = update(opt_state, x, yy, step_size, pixels,  opt_update, get_params, qlayers, n_features)
                train_acc_ = accuracy(params, x, yy, pixels,  qlayers, n_features)
                loss_history.append(float(loss_res))
                train_accuracy.append(float(train_acc_))
                step_size += batch_size
                grad_history.append(gradient)
                # loss_test = loss(params, test_images, test_labels, pixels, encoding)
            print(f'{loss_res=}')
            print(np.mean(train_accuracy))
            a1, a2 = 0, 0
            bs = 64
            for i in range(len(test_images)//bs):
                a2 += bs
                # print(a1, a2)
                loss_history_t.append( loss(params, test_images[a1:a2], test_labels[a1:a2], pixels,  qlayers, n_features))
                # print('loss done')
                test_acc_ = accuracy(params, test_images[a1:a2], test_labels[a1:a2], pixels,  qlayers, n_features,cm)
                # print('acc done')
                test_accuracy_.append(test_acc_)
                a1 = a2
                    
                    
            epoch_time = time.time() - start_time
            # print(f'{params[0]=}  \n {params[1]=} \n ')
            # loss_train = loss(params, train_images, train_labels, pixels, encoding)
            loss_train = np.array(loss_history).mean()
            loss_test = np.array(loss_history_t).mean()
            test_acc = np.array(test_accuracy_).mean()
            loss_history_train.append(float(loss_train))
            loss_history_test.append(float(loss_test))
            test_accuracy.append(float(test_acc))
            params = get_params(opt_state)
            # f.write(f"Epoch {epoch+100} in {epoch_time} sec" + '\n')
            # f.write(f" loss train {loss_history_train[-1]} loss test {loss_history_test[-1]}" + '\n')#Training set accuracy {format(train_acc)}
            # f.write("Test set accuracy {}".format(test_acc)+ '\n')
            print("Epoch {} in {:0.2f} sec".format(epoch, epoch_time))
            print(f" loss {loss_history_train[-1]} loss test {loss_history_test[-1]}")#Training set accuracy {format(train_acc)}
            print("Test set accuracy {}".format(test_acc))
            print('Best test accuracy: ', max(test_accuracy))
        np.save(acc_and_loss, np.array([test_accuracy, cm],dtype=object))
        # np.save(model, params)
        # model.close()
        acc_and_loss.close()
        filename = '1l1f_finalized_model_'+str(pixels)+'_pixels.sav'
        joblib.dump(params, filename)
        # fig, ax = plt.subplots(2, 1)
        # __=ax[0].plot((loss_history_test))
        # _ = ax[1].plot((loss_history_train))
        # xlabel = ax[1].set_xlabel(r'${\rm step\ number}$')
        # ylabel = ax[0].set_ylabel(r'${\rm loss\ test}$')
        # ylabel = ax[1].set_ylabel(r'${\rm loss\ train}$')
        # title = ax[0].set_title(r'${\rm loss\ history}$')
        fig, ax = plt.subplots(1, 1)
        __=ax.plot((loss_history_test), label='test')
        __=ax.plot((loss_history_train), label='train')
        # _ = ax[1].plot((loss_history_train))
        xlabel = ax.set_xlabel(r'${\rm step\ number}$')
        ylabel = ax.set_ylabel(r'${\rm loss}$')
        # ylabel = ax[1].set_ylabel(r'${\rm loss\ train}$')
        title = ax.set_title(r'${\rm loss\ history}$')
        legend = ax.legend()
        fig.savefig('0loss_'+str(pixels)+'_'+str(num_epochs)+'_epochs_2_layer.pdf')
        fig, ax = plt.subplots(1, 1)
        __=ax.plot((test_accuracy), label='test')
        # _ = ax[1].plot((loss_history_train))
        xlabel = ax.set_xlabel(r'${\rm step\ number}$')
        ylabel = ax.set_ylabel(r'${\rm Accuracy}$')
        # ylabel = ax[1].set_ylabel(r'${\rm loss\ train}$')
        title = ax.set_title(r'${\rm accuracy\ history}$')
        legend = ax.legend()
        fig.savefig('0acc_'+str(pixels)+'_'+str(num_epochs)+'_epochs_2_layer.pdf')