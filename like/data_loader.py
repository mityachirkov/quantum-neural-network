# import jax.numpy as jnp
import numpy as np
from torch.utils import data
from torchvision.datasets import MNIST, FashionMNIST
from torchvision.transforms import Resize
from model import one_hot

#

def numpy_collate(batch):
  if isinstance(batch[0], np.ndarray):
    return np.stack(batch)
  elif isinstance(batch[0], (tuple,list)):
    transposed = zip(*batch)
    return [numpy_collate(samples) for samples in transposed]
  else:
    return np.array(batch)

class NumpyLoader(data.DataLoader):
  def __init__(self, dataset, batch_size=1,
                shuffle=False, sampler=None,
                batch_sampler=None, num_workers=0,
                pin_memory=False, drop_last=False,
                timeout=0, worker_init_fn=None):
    super(self.__class__, self).__init__(dataset,
        batch_size=batch_size,
        shuffle=shuffle,
        sampler=sampler,
        batch_sampler=batch_sampler,
        num_workers=num_workers,
        collate_fn=numpy_collate,
        pin_memory=pin_memory,
        drop_last=drop_last,
        timeout=timeout,
        worker_init_fn=worker_init_fn)

class FlattenAndCast(object):
  def __init__(self, pixels):
    self.pixels = pixels
  def __call__(self, pic):
    return np.array(Resize(size=[self.pixels,self.pixels])(pic), dtype=np.float32)
###################################################################################################################
#MNIST
def data(batch_size,n_targets, pixels):
    # Define our dataset, using torch datasets
    mnist_dataset = MNIST('/tmp/mnist/', download=True, transform=FlattenAndCast(pixels))
    print(type(mnist_dataset))
    training_generator = NumpyLoader(mnist_dataset, batch_size=batch_size, num_workers=0)
    
    # Get the full train dataset (for checking accuracy while training)
    # train_images = np.array(Resize(size = [pixels,pixels])(mnist_dataset.train_data), dtype = np.float)
    # train_labels = np.array(mnist_dataset.train_labels[:train_images.shape[0]])
    # Get full test dataset
    mnist_dataset_test = MNIST('/tmp/mnist/', download=True, train=False)
    test_images = np.array(Resize(size = [pixels,pixels])(mnist_dataset_test.test_data), dtype = np.float32)
    test_labels = np.array(mnist_dataset_test.test_labels[:test_images.shape[0]])

    return training_generator,  test_images, test_labels
###################################################################################################################
#BARS AND STRIPES
# def b_n_s(n_samples, height, width, noise_std):
#     """Data generation procedure for 'bars and stripes'.

#     Args:
#         n_samples (int): number of data samples to produce
#         height (int): number of pixels for image height
#         width (int): number of pixels for image width
#         noise_std (float): standard deviation of Gaussian noise added to the pixels
#     """
#     X = np.ones([n_samples, 1, height, width]) * -1
#     y = np.zeros([n_samples])

#     for i in range(len(X)):
#         if np.random.rand() > 0.5:
#             rows = np.where(np.random.rand(width) > 0.5)[0]
#             X[i, 0, rows, :] = 1.0
#             y[i] = -1
#         else:
#             columns = np.where(np.random.rand(height) > 0.5)[0]
#             X[i, 0, :, columns] = 1.0
#             y[i] = +1
#         X[i, 0] = X[i, 0] + np.random.normal(0, noise_std, size=X[i, 0].shape)

#     return X, y

# class bns_dataset(data.Dataset):
#     def __init__(self, X, y, transform=None):
#         """
#         Arguments:
#             csv_file (string): Path to the csv file with annotations.
#             root_dir (string): Directory with all the images.
#             transform (callable, optional): Optional transform to be applied
#                 on a sample.
#         """
#         self.X = X
#         self.y = y
#         self.transform = transform

#     def __len__(self):
#         return len(self.X)
    
#     def __getitem__(self, idx):
#         # if torch.is_tensor(idx):
#         #     idx = idx.tolist()
        
#         # landmarks = self.landmarks_frame.iloc[idx, 1:]
#         # landmarks = np.array([landmarks], dtype=float).reshape(-1, 2)
#         sample =  (self.X[idx],  self.y[idx])

#         if self.transform:
#             sample = self.transform(sample)

#         return sample
# def data(batch_size, n_targets,pixels):
#     bns_dataset_ = bns_dataset(*b_n_s(5000, pixels,pixels,0.001))
#     training_generator = NumpyLoader(bns_dataset_, batch_size=batch_size, num_workers=0)
    
#     # Get the full train dataset (for checking accuracy while training)
#     # train_images = np.array(Resize(size = [pixels,pixels])(mnist_dataset.train_data), dtype = np.float)
#     # train_labels = np.array(mnist_dataset.train_labels[:train_images.shape[0]])
#     # Get full test dataset
#     bns_dataset_test = b_n_s(1000, pixels,pixels,0.001)
#     test_images = np.array(bns_dataset_test[0])
#     test_labels = np.array(bns_dataset_test[1])

#     return training_generator,  test_images, test_labels
###################################################################################################################

def filter_(x, y, first, second):
    keep_f = (y == first) 
    keep_s = (y == second)
    keep = keep_f | keep_s
    f = x[keep_f].shape[0]
    s = x[keep_s].shape[0]
    x, y = x[keep], y[keep]
    if f > s:
      y = y == first
    else:
      y = y == second
    return x, y
