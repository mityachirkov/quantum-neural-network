import numpy as np
from quantum import add_zero_qubit, Circuit
from jax import value_and_grad, jit
from functools import partial

def loss(pred,label):
    #MSE
    compare = np.in1d(label, pred).astype(int)
    mse = compare.sum()/len(label)
    return mse

@partial(jit, static_argnums=(3,4))
def dloss(params,x,y, pixels, encoding):
    dloss = value_and_grad(loss, )(params,x,y, pixels, encoding)
    return dloss

def accuracy(params, images, targets, pixels, encoding, cm=None):
    target_class = jnp.argmax(targets, axis=1)
    pred = predict(params, images, pixels, encoding)
    predicted_class = jnp.argmax(pred, axis=1)
    # conf = confusion_matrix(target_class, predicted_class)
    # if cm != None:
    #     cm.append(conf)
    # print(conf)
    return jnp.mean(predicted_class == target_class)

def update(opt_state, x, y, step, pixels, encoding, optimizer):
    _, opt_update, get_params = optimizer
    params = get_params(opt_state)
    loss_res, grads = dloss(params,x,y, pixels, encoding)
    opt_state = opt_update(step, grads, opt_state)  
    return loss_res, opt_state

def predict(params, image, pixels):
    n = np.log2(pixels)
    shape = 2*np.ones(2*n+1)
    image = image.reshape(-1, *shape)
    image = SV(image)
    image = add_zero_qubit(image)
    circ = Circuit(params, image, qsize=n)


    return result

