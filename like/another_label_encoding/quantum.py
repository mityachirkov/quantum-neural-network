import numpy as np
from qhollab import *

def add_zero_qubit(sv):
    a = sv._data
    b = jnp.zeros_like(a)
    data = jnp.stack((a,b),axis=1)
    return SV(data, ndim=0)

def Сircuit_(theta_, sv, measure = True, qsize = 2):
    if qsize == 4:
        a1 = np.arange(qsize)
        a2 = np.arange(qsize, 2*qsize+1)
        for i in range(num_layers):
            circ *= Qft()[a1[:-1-i]]*Qft()[a2[:-1-i]]
            if i == 0:
                circ *= TF
            else:
                circ *= TF*TF*TF
            circ *= iQft()[a1[:-1-i]]*iQft()[a2[:-1-i]  ]
        if measure == True:
            for i in (a1[:2]+a2[:2]):
                circ *= Measurement(i)
    return circ