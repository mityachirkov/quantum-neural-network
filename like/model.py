import jax.numpy as jnp
from qhollab import simulate,b
from jax.scipy.special import logsumexp
from jax import value_and_grad, jit
from quantum import *
from jax.example_libraries.optimizers import adam, adamax
import jax.debug as jaxd
from sklearn.metrics import confusion_matrix
from functools import partial


def relu(x):
  return jnp.maximum(0, x)

def log_softmax(x):
    c = x.max(axis=-1)[...,None]
    logsumexp = jnp.log(jnp.exp(x - c).sum(axis=-1))[...,None]
    return x - c - logsumexp

def one_hot(x, k, dtype=jnp.float32):
    """Create a one-hot encoding of x of size k."""
    return jnp.array(x[:, None] == jnp.arange(k), dtype)

def accuracy(params, images, targets, pixels, num_layers, n_features,cm=None):
    target_class = jnp.argmax(targets, axis=1)
    pred = predict(params, images, pixels,  num_layers, n_features)
    predicted_class = jnp.argmax(pred, axis=1)
    conf = confusion_matrix(target_class, predicted_class)
    if cm != None:
        cm.append(conf)
    
    return jnp.mean(predicted_class == target_class)

def loss(params, images, targets, pixels,  num_layers, n_features):
    
    preds = predict(params, images, pixels,  num_layers, n_features)
    preds = log_softmax(preds)
    # print(f'{preds.shape=} {targets.shape=}')
    output = -jnp.mean(preds * targets)
    # output = -jnp .mean(-jnp.log(jnp.max(preds, axis=1)))
    return  output

# @partial(jit, static_argnums=(3,4))
def dloss(params,x,y, pixels,  num_layers, n_features):
    dloss = value_and_grad(loss, )(params,x,y, pixels,  num_layers, n_features)
    return dloss

def update(opt_state, x, y, step, pixels,  opt_update, get_params, num_layers, n_features):
    # _, opt_update, get_params = optimizer
    params = get_params(opt_state)
    loss_res, grads = dloss(params,x,y, pixels,  num_layers, n_features)
    # print(grads)
    
    opt_state = opt_update(step, grads, opt_state)  
    return loss_res, opt_state, grads

def qparams(params, pixels, qlayers, features, first_layer=1):
    n = int(np.log2(pixels)-1)
    shape = np.multiply(np.ones(2*n, dtype=int), 2, dtype=int)
    matrix_el = 2**(2*features)-1
    if first_layer==1:
        p1 = np.array(np.pi*np.random.random((matrix_el, *shape)))
        params.append(p1)
    for i in range(first_layer, qlayers):
        for _ in range(2**(i+1)):#для сокращенной версии =3, для полной 2**(2qlayers) -- multiplexor, полной с единичными контролями 2*qlayers
            p2 = np.array(np.pi*np.random.random((matrix_el, *shape[(2*i):])))
            params.append(p2)

def extend_params(old_params, old_num_f, num_f):
    old_letters = permutations('IXYZ', old_num_f)
    old = dict(zip(old_letters, old_params))
    new_letters = permutations('IXYZ', num_f)
    new_params = []
    for it_b in new_letters:
        for it_a in old_letters:
            res = [str(i) for i, j in zip(it_a, it_b) if i == j]
            if(len(res)==old_num_f):
                qwerty = res[0] + res[1]
                new_params.append(old[f'{qwerty}'])
            else:
                new_params.append(0)


# @partial(jit, static_argnums=(2,3))
def predict(params, image, pixels,num_layers, n_features ):
    '''
    params -- array of parameters of network

    pixels -- some power of 2

    encoding -- 'FRQI' or 'Exp'
    '''
    n = int(np.log2(pixels))
    shape = np.multiply(np.ones(2*n+1, dtype=int), 2, dtype=int)
    image = image.reshape(-1, *shape)
    image = SV(image)
    for _ in range(n_features-1):
        image = add_zero_qubit(image)
    circ = Сircuit_(params[2:], image, measure=False, pixels = pixels, num_layers=num_layers, features=n_features )
    image_ = circ(image).measure_all()
    axes = []
    for i in range(1,num_layers):
        axes += [-i, n+n_features-i]
    image_q= jnp.apply_over_axes(jnp.sum, image_.data, axes)
    activations = normalization(image_q, n, n_features, num_layers)
    for w, z in params[:2]:
        outputs = jnp.dot(w, activations.T).T + z
        activations = relu(outputs)
    output = outputs
    return output

def normalization(image, n, n_features, num_layers):
    # assert(isinstance(image,))
    image = image.reshape(-1, int(2**(2*n+n_features-2*(num_layers-1))))
    image_norm = image.sum(axis=-1)
    image_normed = (image/image_norm[:,None]).reshape(-1, int(2**(2*n+n_features-2*(num_layers-1))))

    return image_normed

def add_zero_qubit(sv):
    a = sv._data
    b = jnp.zeros_like(a)
    data = jnp.stack((a,b),axis=1)
    return SV(data, ndim=0)


