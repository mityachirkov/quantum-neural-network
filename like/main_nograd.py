#!/usr/bin/env python3

import time
# import matplotlib.pyplot as plt
# from tqdm import tqdm
from model_nograd import *
from data_loader import *
from layers_init_nograd import *
from jax.example_libraries.optimizers import adam, adamax
pixels = 16
encoding = 1
'encoding: 0 -- exp, 1 -- frqi, 2 -- threshold'
n_targets = 10
sizes = [3*pixels**2, 3*pixels**2//2, pixels**2, pixels, n_targets]
layer_sizes = sizes[2:]

params_ = init_network_params(layer_sizes, random.PRNGKey(0))


step_size = 0.1
num_epochs = 20
num_it = 1
batch_size = 128 #для 32на32 уменьшить батч в 4 раза
num_quantum = 1#len(binaries)

# theta = jnp.asarray(np.pi*np.random.random((int(pixels**2/2),2,2)))
if encoding == 0:
    theta = np.array(np.pi*np.random.random(pixels**2))
    params.insert(0, theta)
if encoding == 1:
    # theta = jnp.asarray(np.pi*np.random.random(2*pixels**2))
    theta = np.array(np.pi*np.random.random(sizes[1]))#для домножения на блочно диагональную матрицу
    params_.insert(0, theta)
    theta = np.array(np.pi*np.random.random(sizes[0]))
    params_.insert(0, theta)
    # params = np.array([theta,linear,linear2])

params = np.array([np.array(xi) for xi in params_])
shapes = tuple([i.shape for i in params_])
# print(f'{shapes=}')
params_ = [i.ravel() for i in params_]
params = jnp.concatenate(params_)
loss_history = []
loss_history_train = []
loss_history_test = []
train_accuracy = []
test_accuracy = []  
step_size = 0
i = 0
# print(f'{params=}')
training_generator,  test_images, test_labels = data(batch_size, n_targets, pixels)

if encoding == 0:
    # train_images = np.exp(1j*np.pi*train_images/255.)
    test_images = np.exp(1j*np.pi*test_images/255.)

if encoding == 1:
    # train_images = np.array([np.sin(np.pi*train_images/255.), np.cos(np.pi*train_images/255.)])
    # train_images = np.swapaxes(train_images,0,1)
    test_images = np.array([np.sin(np.pi*test_images/255.), np.cos(np.pi*test_images/255.)])
    test_images = np.swapaxes(test_images,0,1)

if encoding == 2:
    THRESHOLD = 0.5
    train_images = np.array(train_images > THRESHOLD, dtype=np.int).reshape(-1, train_images.shape[1]*train_images.shape[2])
    test_images = np.array(test_images > THRESHOLD, dtype=np.int).reshape(-1, test_images.shape[1]*test_images.shape[2])

# train_labels = one_hot(train_labels, n_targets)
test_labels = one_hot(test_labels, n_targets)

f = open('nograd_result_'+str(pixels)+'_w_qft_'+str(num_epochs)+'_epochs_'+str(encoding)+'_all_measure_logsoftmax.txt', 'w')
for epoch in range(num_epochs):
    start_time = time.time()
    for x, y in training_generator:
        if encoding == 1:
            x = jnp.array([jnp.sin(np.pi*x/255.), jnp.cos(np.pi*x/255.)])
            x = np.swapaxes(x,0,1)
        if encoding == 0:
            x = np.exp(1j*np.pi*x/255.)
        if encoding == 2:
            x = np.array(x > THRESHOLD, dtype=np.int).reshape(-1, x.shape[1]*x.shape[2])
        for it in range(num_it):
            yy = one_hot(y, n_targets)
            
            # loss_res = loss(params,x,yy,pixels,encoding, shapes)
            loss_res, params_ = update(params, x, yy, pixels, encoding, shapes)
            
            loss_history.append(float(loss_res))
            # if loss_res < 1e-3:
            #     print(f'{loss_res =}, {yy = }')
            params_ = np.array([np.array(xi) for xi in params_])
            shapes = tuple([i.shape for i in params_])
            # print(f'{shapes=}')
            params_ = [i.ravel() for i in params_]
            params = jnp.concatenate(params_)
            step_size += batch_size

    epoch_time = time.time() - start_time
    # params = get_params(opt_state)
    # loss_train = loss(params, train_images, train_labels, theta = None, conv=my_conv)
    # params_ = jnp.array([jnp.array(xi) for xi in params_])
    loss_test = loss(params, test_images, test_labels, pixels, encoding, shapes)
    #loss_history_train.append(float(loss_train))
    # print('labels',train_labels)
    # print('loss test',loss_test)
    # print('loss history',loss_history)
    loss_history_test.append(float(loss_test))
    # train_acc = accuracy(params, train_images, train_labels, pixels, encoding)

    test_acc = accuracy(params, test_images, test_labels, pixels, encoding, shapes)
    # train_accuracy.append(train_acc)
    test_accuracy.append(test_acc)
    f.write(f"Epoch {epoch} in {epoch_time} sec" + '\n')
    f.write(f"Training set accuracy _ loss {loss_history[-1]}" + '\n')
    f.write("Test set accuracy {}".format(test_acc)+ '\n')
    print("Epoch {} in {:0.2f} sec".format(epoch, epoch_time))
    print(f"Training set accuracy _ loss {loss_history[-1]}")
    print("Test set accuracy {}".format(test_acc))
    print('Best test accuracy: ', max(test_accuracy))
f.close()
# fig, ax = plt.subplots(2, 1)
# __=ax[0].plot((loss_history_test))
# _ = ax[1].plot((loss_history), ',')
# xlabel = ax[1].set_xlabel(r'${\rm step\ number}$')
# ylabel = ax[0].set_ylabel(r'${\rm loss\ test}$')
# ylabel = ax[1].set_ylabel(r'${\rm loss\ train}$')
# title = ax[0].set_title(r'${\rm loss\ history}$')
# fig.savefig('nograd_test_loss_'+str(pixels)+'_matrix_multiplication_'+str(num_epochs)+'_epochs_all_measure'+str(encoding)+'s_logsoftmax.pdf')