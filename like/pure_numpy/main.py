#!/usr/bin/env python3

import numpy as np
# import joblib
# from /home/keckikon/quantum-neural-network/like import data_loader.py
from functools import lru_cache, reduce
from itertools import product
from scipy.linalg import expm


downloading_file = '2_layer_finalized_model_16_pixels.sav'
pixels = 16
batch_size = 128
n_targets = 10

# test_images = np.array([np.sin(np.pi*test_images/255.), np.cos(np.pi*test_images/255.)])
# test_images = np.swapaxes(test_images,0,1)
# test_images = np.fft.fft2(test_images)
A = np.pi*np.eye(2)/2
B = np.array([np.sin(A), np.cos(A)])#.swapaxes(0,1).reshape(-1,2,2,2)#
print(np.shape(B), B)
# C = (np.abs(B[:,0,...])**2+np.abs(B[:,1,...])**2).reshape(2,2)
C = (np.abs(B)**2)#.sum(axis=0).reshape(2,2)
print(C)



def Qpart(pic, pixels, qparams):
    n = int(np.log2(pixels))
    shape = 2*np.ones(2*n+1)
    pic = pic.reshape(-1, *shape)
    ###adding new qubit###
    b = np.zeros_like(pic)
    pic = np.stack((pic,b),axis=1)

    for counter, theta in enumerate(qparams):
        
        if counter > 1:
            theta1, theta2, theta3 = theta
            F = np.fft.fftn(pic, n, axes=([-1-counter for i in range(n-counter)]))
            F2 = np.fft.fftn(F, n, axes = ([-1-n-counter for i in range(n-counter)]))
            M1 = CMult(F2, theta1, )
            M2 = CMult(M1, theta2, )
            M = CMult(M2, theta3, )
            F3 = np.fft.fftn(M, n, axes=([-1-counter for i in range(n-counter)]))
            result = np.fft.fftn(F3, n, axes = ([-1-n-counter for i in range(n-counter)]))
        else:
            M = TF(F2, theta)
    result = np.fft.ifftn(M, )
    return result
    
def CMult(phi, params, cond, active, f1):
    if len(cond)==1:
        # phi = phi.swapaxes(cond, -1)#einsum, broadcasting
        # for n, j in enumerate(active[::-1]):
        #     phi=phi.moveaxis(j, -2-n)
    else:
        raise NotImplementedError
    
    funcs = [phi, f1(params)]#I(len(active)+len(cond))
    probs = [np.abs(phi[...,0])**2, np.abs(phi[...,1])**2]
    phi = np.random.choice(funcs, p=probs)

    return phi

def I(x):
    return np.eye(x)

def TF(sv, theta):
        u = SpecialUnitary(2, theta).reshape((len(theta.shape)+3)*[2])
        ndim = len(sv.shape)
        data = np.moveaxis(sv, (ndim,ndim+1), (0,1))
        result = np.einsum('qwkj...,ikj...->iqw...', u, data)
        result = np.moveaxis(result, (0,1), (ndim,ndim+1))
        return result

@lru_cache
def pauli_matrix(num_qubits):
    _pauli_matrices = np.array(
    [[[1, 0], [0, 1]], [[0, 1], [1, 0]], [[0, -1j], [1j, 0]], [[1, 0], [0, -1]]]
    )
    return reduce(np.kron, (_pauli_matrices for _ in range(num_qubits)))[1:]

def SpecialUnitary(num_qubits,theta):
    assert theta.shape[0] == 15
    A = np.tensordot(theta, pauli_matrix(num_qubits), axes=[[0], [0]])
    return expm(1j*A/2)

def relu(x):
  return np.maximum(0, x)

def log_softmax(x):
    c = x.max(axis=-1)[...,None]
    logsumexp = np.log(np.exp(x - c).sum(axis=-1))[...,None]
    return x - c - logsumexp

def Classical(psi, params):
    for w, z in params[7:]:
            outputs = np.dot(w, activations.T).T + z
            activations = relu(outputs)
    return 

def accuracy(params, images, targets, pixels, encoding, cm=None, bi = None, labls=None):
    target_class = np.argmax(targets, axis=1)
    pred = predict(params, images, pixels, encoding)
    predicted_class = np.argmax(pred, axis=1)
    conf = confusion_matrix(target_class, predicted_class)
    if cm != None:
        cm.append(conf)
    if bi != None:
        bi.append(np.arccos(images[predicted_class != target_class,1]))
        #  bi.append(predicted_class)
    if (labls != None):
        p_arr = np.array(predicted_class)
        t_arr = np.array(target_class)
        labls.append((p_arr[predicted_class!= target_class],t_arr[predicted_class!= target_class]))

    return np.mean(predicted_class == target_class)
#контролируемая операция переставляет контролируемый кубит на последнее место, перед ним ставит те, по которым применяется операция
#считаем квадрат модуля коэффициента и применяем тот, что больше