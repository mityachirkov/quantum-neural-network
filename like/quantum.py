import numpy as np
import jax.numpy as jnp
from qhollab import*
from jax.scipy.linalg import block_diag
import itertools
import jax
from jax.scipy.linalg import expm
from functools import lru_cache, reduce
from itertools import product
import time

def Сircuit_(theta_, sv, measure = True, pixels = 2, num_layers = 2, features=2):
    '''
    measure -- initially True; if False turn off measurement of all qubits
    qsize -- amount of qubits for on of coordinate of rectangular picture
    '''
    #???разные икс и игрек брать с разными весами??? в кодировании frqi
    # len_sv = len(sv.data_shape)
    circ = arch_full(theta_, pixels, num_layers, features)
    # print(circ)
    # theta = theta_
    # if qsize == 2:
    #     # theta = theta.reshape(2,2,2,2,2)
    #     index_f = [1,2,3,4]
    #     ###############################################################################################
    #     circ = Qft()[1 ,2]*Qft()[3,4]*Block_diag_matrix(theta[::-1])*iQft()[1,2]*iQft()[3,4]#*X[0]*Qft()[1,2]*Qft()[3,4]*Block_diag_matrix(theta[::-1])*iQft()[1,2]*iQft()[3,4]
    #     #два слоя
    #     # circ = Qft()[2,3]*Qft()[4,5]*Two_features(theta[::-1])*iQft()[2,3]*iQft()[4,5]*Qft()[2]*Qft()[4]*Two_features(theta2)[0,1,2,4]*Qft()[2]*Qft()[4]
    #     # circ = Qft()[1 ,2]*Qft()[3,4]*Block_diag_matrix(theta[::-1])*iQft()[1,2]*iQft()[3,4]*Qft()[1 ,2]*Qft()[3]*CBlock_diag_matrix_1(4, index_f[:-1], theta2)*iQft()[1,2]*iQft()[3]
    #     # circ *= Block_pairwise_mult_circ(index_f, theta, 1)
    #     # circ *= iQft()[1,2]*iQft()[3,4]
    #     # circ = Qft()[0]*Qft()[1 ,2]*Qft()[3,4]*Diag_matrix(theta[::-1])*iQft()[0]*iQft()[1,2]*iQft()[3,4]
    #     # circ = Qft()[0,1]*Qft()[2,3]*Diag_matrix(theta)*iQft()[0,1]*iQft()[2,3]#iQft 
    #     # circ = Qft()[0,1]*Qft()[2,3]*CMult(0, [1,2,3], theta[:3])*CMult(1, [0,2,3], theta[3:6])*CMult(2, [0,2,3], theta[6:9])*CMult(3, [0,1,2], theta[3])*iQft()[0,1]*iQft()[2,3]
    #     # circ = Qft()[0,1]*Qft()[2,3]*CMult(0, [1,2,3], theta[0,...])*CMult(1, [0,2,3], theta[:,0,...])*CMult(2, [0,1,3], theta[...,0,:])*CMult(3, [0,1,2], theta[...,0])*iQft()[0,1]*iQft()[2,3]
    #     ###############################################################################################
    # if qsize == 3:
    #     theta, theta2, theta3,theta4 = theta
    #     index_f = [0,1,2,3,4,5,6]
    #     index_s = [0,1,2,4,5]
    #     ###############################################################################################
    #     #1 layer, 0 features
    #     # circ = Qft()[0,1,2]*Qft()[3,4,5]*Diag_matrix(theta)*iQft()[0,1,2]*iQft()[3,4,5]
    #     ###############################################################################################
    #     #1 layer, 1 feature
    #     # circ = Qft()[1 ,2,3]*Qft()[4,5,6]*Block_diag_matrix(theta[::-1])[0,1,2,3,4,5,6]*iQft()[1 ,2,3]*iQft()[4,5,6]
    #     ###############################################################################################
    #     #2 layers, 1 feature
    #     # circ = Qft()[1 ,2,3]*Qft()[4,5,6]*Block_diag_matrix(theta[::-1])[0,1,2,3,4,5,6]*iQft()[1 ,2,3]*iQft()[4,5,6]*Qft()[1 ,2]*Qft()[4,5]*CBlock_diag_matrix_1(index_f[-1], index_s, theta2, None)*CBlock_diag_matrix_1(index_f[qsize], index_s, theta3, None)*CBlock_diag_matrix_0(index_f[-1], index_s, theta4, None)*iQft()[0]*iQft()[1 ,2]*iQft()[4,5]
    #     ###############################################################################################
    #     #1 layer, 2 features
    #     # circ = Qft()[2,3,4]*Qft()[5,6,7]*Two_features(theta[::-1])*iQft()[2,3,4]*iQft()[5,6,7]
    #     ###############################################################################################
    #     #2 layers, 2 features
    #     circ = Qft()[2,3,4]*Qft()[5,6,7]*Two_features(theta[::-1])*iQft()[2,3,4]*iQft()[5,6,7]*Qft()[2,3]*Qft()[5,6]*CTwo_features_1(index_f[-1], index_s, theta2, None)*CTwo_features_1(index_f[qsize], index_s, theta3, None)*CTwo_features_0(index_f[-1], index_s, theta4, None)*iQft()[2,3]*iQft()[5,6]
    #     ###############################################################################################
    #     #3 layers, 2 features
    #     # circ = Qft()[2,3,4]*Qft()[5,6,7]*Two_features(theta[::-1])*iQft()[2,3,4]*iQft()[5,6,7]*Qft()[2,3]*Qft()[5,6]*CTwo_features_1(index_f[-1], index_s, theta2, None)*CTwo_features_1(index_f[qsize], index_s, theta3, None)*CTwo_features_0(index_f[-1], index_s, theta4, None)*iQft()[2,3]*iQft()[5,6]*Qft()[2]*Qft()[5]*CTwo_features_1(index_f[-1], index_s, theta5, None)*CTwo_features_1(index_f[qsize], index_s, theta6, None)*CTwo_features_0(index_f[-1], index_s, theta7, None)*iQft()[2]*iQft()[5]
    #     ###############################################################################################
    # if qsize == 4:
    #     size = 1
    #     # index_f = [0,1,2,3,4,5,6,7,8,9]#
    #     # index_s = [0,1,2,3,4,6,7,8]
    #     index_t = [0,1,2,3,6,7]
    #     # index_pairs = ring(index_f, size)
    #     # index_pairs1 = ring(index_f, 1)
    #     # index_pairs2 = ring(index_s, size)
    #     #первый слой двухкубитное диагональное
    #     ###############################################################################################
    #     #1layer, 0 features
    #     # circ = Qft()[0,1,2,3]*Qft()[4, 5,6,7]*Diag_matrix(theta)*iQft()[0,1,2,3]*iQft()[4,5,6,7] #базовое домножение на диагональную матрицу для exp
    #     ###############################################################################################
    #     #
    #     # circ = Qft()[1 ,2,3,4]*Qft()[5,6,7,8]
    #     # circ *= Block_pairwise_mult_circ(circ, index_f, theta,2)
    #     # circ *= iQft()[1 ,2,3,4]*iQft()[5,6,7,8]
    #     ###############################################################################################
    #     #1 layer, 1 feature
    #     # circ = Qft()[1 ,2,3,4]*Qft()[5,6,7,8]*Block_diag_matrix(theta[::-1])[0,1,2,3,4,5,6,7,8]*iQft()[1 ,2,3,4]*iQft()[5,6,7,8]
    #     ###############################################################################################
    #     #2 layers, 1 feature
    #     # index_f = [0,1,2,3,4,5,6,7,8]#
    #     # index_s = [0,1,2,3,5,6,7]
        
    #     # circ = Qft()[1 ,2,3,4]*Qft()[5,6,7,8]*Block_diag_matrix(theta[::-1])*iQft()[1 ,2,3,4]*iQft()[5,6,7,8]*Qft()[1 ,2,3]*Qft()[5,6,7]*CBlock_diag_matrix_1(index_f[-1], index_s, theta2, None)*CBlock_diag_matrix_1(index_f[qsize], index_s, theta3, None)*CBlock_diag_matrix_0(index_f[-1], index_s, theta4, None)*iQft()[1 ,2,3]*iQft()[5,6,7]
        
    #     ###############################################################################################
    #     #1 layer, 2 features
    #     # circ = Qft()[2,3,4,5]*Qft()[6,7,8,9]*Two_features(theta[::-1])[0,1,2,3,4,5,6,7,8,9]*iQft()[2,3,4,5]*iQft()[6,7,8,9]
    #     ###############################################################################################
    #     #2 layers, 2 features
    #     # theta, theta2, theta3,theta4 = theta_
    #     # index_f = [0,1,2,3,4,5,6,7,8,9]#
    #     # index_s = [0,1,2,3,4,6,7,8]
    #     # # circ = Qft()[2,3,4,5]*Qft()[6,7,8,9]*Two_features(theta[::-1])*iQft()[2,3,4,5]*iQft()[6,7,8,9]*Qft()[2,3,4]*Qft()[6,7,8]*CTwo_features_10(index_f[-1], index_f[qsize+1], index_s, theta2, None)*CTwo_features_01(index_f[-1], index_f[qsize+1], index_s, theta3, None)*CTwo_features_00(index_f[-1], index_f[qsize+1], index_s, theta4, None)*iQft()[2,3,4]*iQft()[6,7,8]
    #     # circ = Qft()[2,3,4,5]*Qft()[6,7,8,9]*Two_features(theta[::-1])*iQft()[2,3,4,5]*iQft()[6,7,8,9]*Qft()[2,3,4]*Qft()[6,7,8]*CTwo_features_0(index_f[qsize+1], index_s, theta2, None)*CTwo_features_1(index_f[-1], index_s, theta3, None)*CTwo_features_0(index_f[-1], index_s, theta4, None)*iQft()[2,3,4]*iQft()[6,7,8]
    #     ###############################################################################################
    #     #3 layers, 2 features
    #     # theta, theta2, theta3,theta4,theta5, theta6, theta7 = theta_
    #     # index_f = [0,1,2,3,4,5,6,7,8,9]#
    #     # index_s = [0,1,2,3,4,6,7,8]
    #     # circ = Qft()[2,3,4,5]*Qft()[6,7,8,9]*Two_features(theta[::-1])[0,1,2,3,4,5,6,7,8,9]*iQft()[2,3,4,5]*iQft()[6,7,8,9]*Qft()[2,3,4]*Qft()[6,7,8]*CTwo_features_1(index_f[-1], index_s, theta2, None)*CTwo_features_1(index_f[qsize], index_s, theta3, None)*CTwo_features_0(index_f[-1], index_s, theta4, None)*iQft()[2,3,4]*iQft()[6,7,8]*Qft()[2,3]*Qft()[6,7]*CTwo_features_1(index_f[-2], index_t, theta5, None)*CTwo_features_1(index_f[qsize-1], index_t, theta6, None)*CTwo_features_0(index_f[-2], index_t, theta7, None)*iQft()[2,3]*iQft()[6,7]
    #     ###############################################################################################
    #     #5 layers, 4 features
    #     theta, theta2, theta3,theta4,theta5, theta6, theta7, theta8, theta9, theta10, theta11, theta12, theta13 = theta_
    #     index_f = [0,1,2,3,4,5,6,7,8,9,10,11]#
    #     index_s = [0,1,2,3,4,5,6,8,9,10]
    #     index_3 = [0,1,2,3,4,5,8,9]
    #     index_4 = [0,1,2,3,4,8]
    #     index_5 = [0,1,2,3]
    #     circ = Qft()[4,5,6,7]*Qft()[8,9,10,11]*Four_features(theta[::-1])[0,1,2,3,4,5,6,7,8,9]*iQft()[4,5,6,7]*iQft()[8,9,10,11]*Qft()[4,5,6]*Qft()[8,9,10]*\
    #     CFour_features_1(index_f[-1], index_s, theta2, None)*CFour_features_1(index_f[qsize+3], index_s, theta3, None)*CFour_features_0(index_f[-1], index_s, theta4, None)*\
    #     iQft()[4,5,6]*iQft()[8,9,10]*Qft()[4,5]*Qft()[8,9]*CFour_features_1(index_f[-2], index_3, theta5, None)*CFour_features_1(index_f[qsize+2], index_3, theta6, None)*\
    #     CFour_features_0(index_f[-2], index_3, theta7, None)*iQft()[4,5]*iQft()[8,9]*H[4]*H[8]*CFour_features_1(index_f[-3], index_4, theta8, None)*\
    #     CFour_features_1(index_f[qsize+1], index_4, theta9, None)*CFour_features_0(index_f[-3], index_4, theta10, None)*H[4]*H[8]*CFour_features_1(index_f[-4], index_5, theta11, None)*\
    #     CFour_features_1(index_f[qsize], index_5, theta12, None)*CFour_features_0(index_f[-4], index_5, theta13, None)
    #     ###############################################################################################
    # if qsize ==5:
    #     size = 1
    #     index_f = [0,1,2,3,4,5,6,7,8,9,10,11]
    #     index_s = [0,1,2,3,4,5,7,8,9,10]
    #     index_t = [0,1,2,3,4,7,8,9]
    #     index_4 = [0,1,2,3,7,8]
    #     index_5 = [0,1,2,7]
    #     theta, theta2, theta3,theta4,theta5, theta6, theta7 = theta_
    #     # index_pairs = ring(index_f, size)
    #     # index_pairs1 = ring(index_f, 1)
    #     # index_pairs2 = ring(index_s, size)
    #     ###############################################################################################
    #     #1 layer, 1 feature
    #     # circ = Qft()[1,2,3,4,5]*Qft()[6,7,8,9,10]*Block_diag_matrix(theta[::-1])*iQft()[1,2,3,4,5]*iQft()[6,7,8,9,10]
    #     ###############################################################################################
    #     #2 layers, 1 feature
    #     # index_f = [0,1,2,3,4,5,6,7,8,9,10]
    #     # index_s = [0,1,2,3,4,6,7,8,9]
    #     # circ = Qft()[1,2,3,4,5]*Qft()[6,7,8,9,10]*Block_diag_matrix(theta[::-1])*iQft()[1,2,3,4,5]*iQft()[6,7,8,9,10]*Qft()[1 ,2,3,4]*Qft()[6,7,8,9]*CBlock_diag_matrix_1(index_f[-1], index_s, theta2, None)*CBlock_diag_matrix_1(index_f[qsize], index_s, theta3, None)*CBlock_diag_matrix_0(index_f[-1], index_s, theta4, None)*iQft()[1 ,2,3,4]*iQft()[6,7,8,9]
    #     ###############################################################################################
    #     #1 layer, 2 features
    #     # circ = Qft()[2,3,4,5,6]*Qft()[7,8,9,10,11]*Two_features(theta[::-1])*iQft()[2,3,4,5,6]*iQft()[7,8,9,10,11]
    #     ###############################################################################################
    #     #2 layers, 2 features
        
    #     # circ = Qft()[2,3,4,5,6]*Qft()[7,8,9,10,11]*Two_features(theta[::-1])*iQft()[2,3,4,5,6]*iQft()[7,8,9,10,11]*Qft()[2,3,4,5]*Qft()[7,8,9,10]*CTwo_features_0(index_f[-1], index_s,theta2[::-1], None)*CTwo_features_0(index_f[qsize+1], index_s, theta3[::-1], None)*CTwo_features_1(index_f[-1], index_s,theta4[::-1], None)*iQft()[2,3,4,5]*iQft()[7,8,9,10]
    #     ###############################################################################################
        
        
        # circ = Qft()[2,3,4,5,6]*Qft()[7,8,9,10,11]*Two_features(theta[::-1])*iQft()[2,3,4,5,6]*iQft()[7,8,9,10,11]*Qft()[2,3,4,5]*Qft()[7,8,9,10]*\
        # CTwo_features_0(index_f[-1], index_s,theta2[::-1], None)*CTwo_features_0(index_f[qsize+1], index_s, theta3[::-1], None)*\
        # CTwo_features_1(index_f[-1], index_s,theta4[::-1], None)*iQft()[2,3,4,5]*iQft()[7,8,9,10]*Qft()[2,3,4]*Qft()[7,8,9]*\
        # CTwo_features_0(index_f[-2], index_t,theta5[::-1], None)*CTwo_features_0(index_f[qsize], index_t, theta6[::-1], None)\
        # *CTwo_features_1(index_f[-2], index_t,theta7[::-1], None)*iQft()[2,3,4]*iQft()[7,8,9]
    #     ###############################################################################################
    #     #4 layers, 2 features
    #     # circ = Qft()[2,3,4,5,6]*Qft()[7,8,9,10,11]*Two_features(theta[::-1])*iQft()[2,3,4,5,6]*iQft()[7,8,9,10,11]*Qft()[2,3,4,5]*Qft()[7,8,9,10]*\
    #     # CTwo_features_0(index_f[-1], index_s,theta2[::-1], None)*CTwo_features_0(index_f[qsize+1], index_s, theta3[::-1], None)*\
    #     # CTwo_features_1(index_f[-1], index_s,theta4[::-1], None)*iQft()[2,3,4,5]*iQft()[7,8,9,10]*Qft()[2,3,4]*Qft()[7,8,9]*\
    #     # CTwo_features_0(index_f[-2], index_t,theta5[::-1], None)*CTwo_features_0(index_f[qsize], index_t, theta6[::-1], None)\
    #     # *CTwo_features_1(index_f[-2], index_t,theta7[::-1], None)*iQft()[2,3,4]*iQft()[7,8,9]*Qft()[2,3]*Qft()[7,8]*\
    #     # CTwo_features_0(index_f[-3], index_4,theta8[::-1], None)*CTwo_features_0(index_f[qsize-1], index_4, theta9[::-1], None)\
    #     # *CTwo_features_1(index_f[-3], index_4,theta10[::-1], None)*iQft()[2,3]*iQft()[7,8]
    #     ###############################################################################################
    #     #5 layers, 2 features
    #     # circ = Qft()[2,3,4,5,6]*Qft()[7,8,9,10,11]*Two_features(theta[::-1])*iQft()[2,3,4,5,6]*iQft()[7,8,9,10,11]*Qft()[2,3,4,5]*Qft()[7,8,9,10]*\
    #     # CTwo_features_0(index_f[-1], index_s,theta2[::-1], None)*CTwo_features_0(index_f[qsize+1], index_s, theta3[::-1], None)*\
    #     # CTwo_features_1(index_f[-1], index_s,theta4[::-1], None)*iQft()[2,3,4,5]*iQft()[7,8,9,10]*Qft()[2,3,4]*Qft()[7,8,9]*\
    #     # CTwo_features_0(index_f[-2], index_t,theta5[::-1], None)*CTwo_features_0(index_f[qsize], index_t, theta6[::-1], None)\
    #     # *CTwo_features_1(index_f[-2], index_t,theta7[::-1], None)*iQft()[2,3,4]*iQft()[7,8,9]*Qft()[2,3]*Qft()[7,8]*\
    #     # CTwo_features_0(index_f[-3], index_4,theta8[::-1], None)*CTwo_features_0(index_f[qsize-1], index_4, theta9[::-1], None)\
    #     # *CTwo_features_1(index_f[-3], index_4,theta10[::-1], None)*iQft()[2,3]*iQft()[7,8]*Qft()[2]*Qft()[7]*\
    #     # CTwo_features_0(index_f[-4], index_5,theta11[::-1], None)*CTwo_features_0(index_f[qsize-2], index_5, theta12[::-1], None)\
    #     # *CTwo_features_1(index_f[-4], index_5,theta13[::-1], None)*iQft()[2,3]*iQft()[7,8]
    #     ###############################################################################################
    #     # layers, 4 features
    #     # index_f = [0,1,2,3,4,5,6,7,8,9,10,11,12,13]
    #     # index_s = [0,1,2,3,4,5,6,7,9,10,11,12]
    #     # index_t = [0,1,2,3,4,5,6,9,10,11]
    #     # index_4 = [0,1,2,3,4,5,9,10]
    #     # index_5 = [0,1,2,3,4,9]
    #     # index_6 = [0,1,2,3]
    #     # theta, theta2, theta3,theta4,theta5, theta6, theta7, theta8, theta9, theta10, theta11, theta12, theta13, theta14, theta15, theta16 = theta_
    #     # circ = Qft()[4,5,6,7,8]*Qft()[9,10,11,12,13]*Four_features(theta[::-1])*iQft()[4,5,6,7,8]*iQft()[9,10,11,12,13]*Qft()[4,5,6,7]*Qft()[9,10,11,12]*\
    #     # CFour_features_0(index_f[-1], index_s,theta2[::-1], None)*CFour_features_0(index_f[qsize+3], index_s, theta3[::-1], None)*\
    #     # CFour_features_1(index_f[-1], index_s,theta4[::-1], None)*iQft()[4,5,6,7]*iQft()[9,10,11,12]*Qft()[4,5,6]*Qft()[9,10,11]*\
    #     # CFour_features_0(index_f[-2], index_t,theta5[::-1], None)*CFour_features_0(index_f[qsize+2], index_t, theta6[::-1], None)\
    #     # *CFour_features_1(index_f[-2], index_t,theta7[::-1], None)*iQft()[4,5,6]*iQft()[9,10,11]*Qft()[4,5]*Qft()[9,10]*\
    #     # CFour_features_0(index_f[-3], index_4,theta8[::-1], None)*CFour_features_0(index_f[qsize+1], index_4, theta9[::-1], None)\
    #     # *CFour_features_1(index_f[-3], index_4,theta10[::-1], None)*iQft()[4,5]*iQft()[9,10]*Qft()[4]*Qft()[9]*\
    #     # CFour_features_0(index_f[-4], index_5,theta11[::-1], None)*CFour_features_0(index_f[qsize], index_5, theta12[::-1], None)\
    #     # *CFour_features_1(index_f[-4], index_5,theta13[::-1], None)*iQft()[4]*iQft()[9]*CFour_features_0(index_f[-5], index_6,theta14[::-1], None)*CFour_features_0(index_f[qsize-1], index_6, theta15[::-1], None)\
    #     # *CFour_features_1(index_f[-5], index_6,theta16[::-1], None)
    #     ###############################################################################################
    if measure == True:
            # for i in (a1[:2]+a2[:2]):
            for i in index_f:
                circ *= Measurement(i)
    return circ

def arch(params,pixels, qlayers, features):
    '''
    Архитектура с использыванием одной пары измерений в каждом слое
    '''
    n = int(np.log2(pixels))+features
    qubits = 2*n-features
    indexes = [i for i in range(qubits)]
    temp_ind = np.arange(qubits)
    # print(f'{SpecialUnitary(features, params[0])=}')
    circ = Qft()[(*indexes[features:n],)]*Qft()[(*indexes[n:],)]*iQft()[(*indexes[features:n],)]*iQft()[(*indexes[n:],)]
    circ*=N_features(params[0],features)*iQft()[(*indexes[features:n],)]*iQft()[(*indexes[n:],)]
    for i in range(1,qlayers):
        temp_ind = np.delete(temp_ind, (n-i, -1))
        circ *= Qft()[(*indexes[features:n-i],)]*Qft()[(*indexes[n:-i],)]
        circ *= CN_features_0(indexes[-i], temp_ind, params[3*(i-1)+1],features)*CN_features_1(indexes[n-i], temp_ind, params[3*(i-1)+2],features)*CN_features_1(indexes[-i], temp_ind, params[3*(i-1)+3],features)
        circ *= iQft()[(*indexes[features:n-i],)]*iQft()[(*indexes[n:-i],)]
    # print(circ)
    return circ

def arch_full(params,pixels, qlayers, features):
    '''
    Архитектура с использыванием всех измерений в каждом слое
    '''
    n = int(np.log2(pixels))+features
    qubits = 2*n-features
    indexes = [i for i in range(qubits)]
    classical_ind = []
    param_ind = 1
    temp_ind = np.arange(qubits)
    circ = Qft()[(*indexes[features:n],)]*Qft()[(*indexes[n:],)]
    circ*=N_features(params[0],features)*iQft()[(*indexes[features:n],)]*iQft()[(*indexes[n:],)]
    for i in range(1,qlayers):
        temp_ind = np.delete(temp_ind, (n-i, -1))
        measurement_comb = itertools.product('01', repeat = i+1)
        classical_ind.append(n-i)
        classical_ind.append(qubits-i)
        circ *= Qft()[(*indexes[features:n-i],)]*Qft()[(*indexes[n:-i],)]
        for comb in  measurement_comb:
            circ *= Condition(N_features(params[param_ind], features), cond=dict(zip(classical_ind, np.array(comb, dtype=int))), indices=[*temp_ind])
            param_ind += 1
        circ *= iQft()[(*indexes[features:n-i],)]*iQft()[(*indexes[n:-i],)]
    return circ


class Diag_matrix(Circuit):
    def __init__(self, theta):
        super().__init__(name='MAT') 
        self.theta = theta.reshape(-1)
    
    def apply(self, sv):
        # print('sv')
        assert isinstance(sv, StateVector)
        self.init(sv.ss)
        matrix = jnp.exp(-1j*self.theta)
        result = sv._data.reshape(-1,self.ss.dim) * matrix
        return SV(
            data=result.reshape(sv._data.shape),
            ndim=sv._ndim
        )
    def inverse(self):
        return self

class iQft(Circuit):
    def __init__(self):
        super().__init__(name='iQFT') 
        
    def apply(self, sv):
        assert isinstance(sv, StateVector)
        self.init(sv.ss)

        if isinstance(sv, SV):
            result = jnp.fft.ifft(sv._data.reshape((-1,sv.ss.dim)), norm="ortho")
            # print('DONE iQFT!')
            return SV(
                data=result.reshape(sv._data.shape),
                ndim=sv._ndim
            )
        raise NotImplementedError

def CDiag_matrix_0(c, i,theta): return Condition(Diag_matrix(theta[::-1]), cond={c:0}, indices=[*i])

def CDiag_matrix_1(c, i,theta): return Condition(Diag_matrix(theta[::-1]), cond={c:1}, indices=[*i])

class Block_diag_matrix(Circuit):
    def __init__(self, theta, vector_n = None):
        super().__init__(name='BMAT') 
        self.theta = theta#.reshape(-1,3)
        self.vector_n = vector_n
        # self.u = SpecialUnitary(1, theta)#jnp.array([U3(t) for t in self.theta])
    
    def apply(self, sv):
        assert isinstance(sv, StateVector)
        self.init(sv.ss)
        ndim = sv.ndim

        # ndim=0
        # print(ndim, sv._data.shape)
        # data = jnp.moveaxis(sv._data, (ndim,ndim+1), (0,1))

        # u = self.u.reshasv._datape(*sv._data.shape[ndim+1:])
        # print(f'{data.shape=} {sv.ss=}')

        u = SpecialUnitary(1, self.theta)


        # result = jnp.einsum('qk...,ikw...->iqw...', u, data)
        # print(result.shape)
        result = jnp.stack([u[0,0,...]*sv._data[:,0,...]+u[0,1,...]*sv._data[:,1,...],u[1, 0,...]*sv._data[:,0,...]+u[1,1,...]*sv._data[:,1,...]], axis = 1)
        # result = jnp.moveaxis(result, (0,1), (ndim,ndim+1))
        # result = jnp.stack([u[0,0,...]*sv._data[:,0,...]+u[0,1,...]*sv._data[:,1,...],u[1, 0,...]*sv._data[:,0,...]+u[1,1,...]*sv._data[:,1,...]], axis = 1)
        # print(result.shape)
        return SV(
            data=result,
            ndim=sv._ndim)
    
    def inverse(self):
        return self
    

class Two_features(Circuit):
    def __init__(self, theta, vector_n = None):
        super().__init__(name='tf') 
        self.theta = theta#(6,2,2,2,..,2)
        # self.u = U6(self.theta)
        # print(len(self.theta.shape))
        self.u = SpecialUnitary(2, self.theta).reshape((len(self.theta.shape)+3)*[2])#(15,2,2,2,..,2)
        # print(self.u.shape)
        # self.init(SS(self.u.shape[2:]))
    
    def apply(self, sv):
        assert isinstance(sv, StateVector)
        # self.init(sv.ss)
        
        # print("DONE!")
        ndim = sv.ndim
        # print(f"{ndim=}")
        # print(f'{sv._data.reshape(-1,32,32)=}')
        data = jnp.moveaxis(sv._data, (ndim,ndim+1), (0,1))
        print(data.shape)
        print(self.u.shape)
        result = jnp.einsum('qwkj...,ikj...->iqw...', self.u, data)
        result = jnp.moveaxis(result, (0,1), (ndim,ndim+1))
        return SV(
            data=result,
            ndim=ndim)
    
class N_features(Circuit):
    def __init__(self, theta, n_features = None):
        super().__init__(name='tf') 
        self.theta = theta#(255,2,2,2,..,2)
        self.n = 2**n_features
        self.u = jnp.moveaxis(SpecialUnitary(n_features, self.theta), (0),(-1)).reshape(self.n, self.n, *theta.shape[1:])#.reshape((len(self.theta.shape)+7)*[2])#(16,16,2,2,2,2....)
        self.n_features = n_features
        # print(self.u.shape)
        # self.init(SS(self.u.shape[2:]))
    
    def apply(self, sv):
        assert isinstance(sv, StateVector)
        # self.init(sv.ss)
        
        # print("DONE!")
        ndim = sv.ndim
        data = sv._data
        #u - diag ничего не должно меняется, своп
        data = jnp.reshape(data, (np.prod(data.shape[:ndim+1], dtype=int), 2**self.n_features, *data.shape[self.n_features+ndim+1:]))
        result = jnp.einsum('qw...,iw...->iq...', self.u, data)
        result = jnp.reshape(result, sv._data.shape)
        # print(f'{result=}')
        # print(f'{(np.real(result)**2+np.imag(result)**2).sum()=}')
        return SV(
            data=result,
            ndim=ndim)
# class two_features(Circuit):
#     def __init__(self, theta, vector_n = None):4
#         super().__init__(name='tf') 
#         self.theta = theta.reshape(-1,6)
#         self.vector_n = vector_n
    
#     def apply(self, sv):
#         assert isinstance(sv, StateVector)
#         self.init(sv.ss)
#         # print(f'{sv._data.shape=} {sv.ss=}')
#         u = jnp.array([U6(t) for t in self.theta])
#         # print("U6!!!!!!!!")
#         print(f'{sv._data.shape=} {sv.ss=} {sv.ss.dim=}')
#         if self.vector_n == None:
#             i=2
#             c=0
#             while i==2:
#                 i = sv._data.shape[c]
#                 print(sv._data.shape[c])
#                 c+=1
#             print(c,i)
#             # c -=1
#             u = u.reshape(*sv._data.shape[c:])
#         else:
#             shap = 2*np.ones(self.vector_n, dtype= int)
#             u = u.reshape(*shap,2,2)
#             # u = u.reshape(*sv._data.shape[1:],2)
#         # print(f'{u.shape=} {sv._data.shape=}')
#         #result = jnp.stack([u[0,0,...]*sv._data[:,0,...]+u[0,1,...]*sv._data[:,1,...],u[1, 0,...]*sv._data[:,0,...]+u[1,1,...]*sv._data[:,1,...]], axis = 1)
#         result = jnp.einsum('lkj...,ikj...', u,sv._data)
#         return SV(
#             data=result,
#             ndim=sv._ndim)
    
#     def inverse(self):
#         return self

def CBlock_diag_matrix_0(c, i,theta, vector_n): return Condition(Block_diag_matrix(theta[::-1], vector_n), cond={c:0}, indices=[*i])

def CBlock_diag_matrix_1(c, i,theta, vector_n): return Condition(Block_diag_matrix(theta[::-1], vector_n), cond={c:1}, indices=[*i])

def CTwo_features_0(c, i,theta, vector_n): return Condition(Two_features(theta[::-1], vector_n), cond={c:0}, indices=[*i])

def CTwo_features_00(c, c1, i,theta, vector_n): return Condition(Two_features(theta[::-1], vector_n), cond={c:0,c1:0}, indices=[*i])

def CTwo_features_01(c, c1, i,theta, vector_n): return Condition(Two_features(theta[::-1], vector_n), cond={c:0,c1:1}, indices=[*i])

def CTwo_features_10(c, c1, i,theta, vector_n): return Condition(Two_features(theta[::-1], vector_n), cond={c:1,c1:0}, indices=[*i])

def CTwo_features_1(c, i,theta, vector_n): return Condition(Two_features(theta[::-1], vector_n), cond={c:1}, indices=[*i])

def CN_features_0(c, i,theta, features): return Condition(N_features(theta[::-1], features), cond={c:0}, indices=[*i])

def CN_features_1(c, i,theta, features): return Condition(N_features(theta[::-1], features), cond={c:1}, indices=[*i])

def Second_cond_0(A, c, i): return Condition(A, cond={c:0}, indices=A._indices)
def Second_cond_1(A, c, i): return Condition(A, cond={c:1}, indices=[*i])

# def U3(ang): 
#     '''
#     ang -- array of (3,...)
#     '''
#     t,p,l = ang
    
#     return jnp.asarray([    [jnp.cos(t/2), -jnp.exp(1j*l)*jnp.sin(t/2)],
#                 [jnp.exp(1j*p)*jnp.sin(t/2), jnp.exp(1j*(p+l))*jnp.cos(t/2)]    ])
# @lru_cache
def U3(ang):
    t,p,l = ang
    cos_half_t = jnp.cos(t / 2)
    sin_half_t = jnp.sin(t / 2)
    exp_ip = jnp.exp(1j * p)
    exp_il = jnp.exp(1j * l)
    
    matrix = jnp.stack([
        jnp.asarray([cos_half_t, -exp_il * sin_half_t]),
        jnp.asarray([exp_ip * sin_half_t, exp_ip * exp_il * cos_half_t])
    ])
    
    return jnp.asarray(matrix)

@lru_cache
def pauli_matrix(num_qubits):
    _pauli_matrices = jnp.array(
    [[[1, 0], [0, 1]], [[0, 1], [1, 0]], [[0, -1j], [1j, 0]], [[1, 0], [0, -1]]]
    )
    return reduce(jnp.kron, (_pauli_matrices for _ in range(num_qubits)))[1:]

def SpecialUnitary(num_qubits,theta): 
    # assert theta.shape[0] == 15
    theta = theta.reshape(-1, 4**num_qubits-1)
    A_ = jnp.tensordot(theta, pauli_matrix(num_qubits), axes=[[-1], [0]])
    
    # A =  jnp.moveaxis(A_, (-2,-1), (0,1))
    # print(f'{A.shape= }{pauli_matrix(num_qubits).shape=}{theta.shape=}{A=}')
    return expm(1j*A_)

def Pairwise_mult_circ(circ, index, theta):
    pair_order_list = itertools.combinations(index,2)
    q = list(pair_order_list)
    theta = theta.reshape(-1,1)
    for n, i in enumerate(q):
        circ*=Diag_matrix(theta[n])[i]
    # circ *= iQft()[1 ,2]*iQft()[3,4]
    return circ 

def Block_pairwise_mult_circ(circ, q, theta, pair_n):
    print(f'{q=}')
    theta = theta.reshape(-1,3*(2**pair_n))
    for n, i in enumerate(q):
        t = tuple(i)
        circ*=Block_diag_matrix(theta[n],pair_n)[t]
    # circ *= iQft()[1 ,2]*iQft()[3,4]
    # print(circ)
    return circ 

def CBlock_pairwise_mult_circ(circ, index_pairs, c_index, theta, pair_n): 
    theta = theta.reshape(-1,3*(2**pair_n))
    for n, i in enumerate(index_pairs):

        circ*=CBlock_diag_matrix_1(c_index,i,theta[n],pair_n)
    # circ *= iQft()[1 ,2]*iQft()[3,4]
    # print(circ)
    return circ 

def circslice(l, a, b):
    if b>=a:
        return l[a:b]
    else:
        return l[a:]+l[:b]
    
def all_pairs(A,size):
    pair_order_list = itertools.combinations(A,size)
    q = list(pair_order_list) 
    return q

def ring(A, size):
    '''
    A -- list of indices
    size -- amount of connections
    '''
    ring_pairs = []
    for i in range(0,len(A)):
        if (i+size)>len(A):
            t = circslice(A, i, i+size-len(A))
        else: t = circslice(A, i, i+size)
        ring_pairs.append(t)

    return ring_pairs




