# import jax.numpy as jnp
import numpy as np
from torch.utils import data
from torchvision.datasets import MNIST, FashionMNIST
from torchvision.transforms import Resize
from model import one_hot

#parameters of downloadings

def numpy_collate(batch):
  if isinstance(batch[0], np.ndarray):
    return np.stack(batch)
  elif isinstance(batch[0], (tuple,list)):
    transposed = zip(*batch)
    return [numpy_collate(samples) for samples in transposed]
  else:
    return np.array(batch)

class NumpyLoader(data.DataLoader):
  def __init__(self, dataset, batch_size=1,
                shuffle=False, sampler=None,
                batch_sampler=None, num_workers=0,
                pin_memory=False, drop_last=False,
                timeout=0, worker_init_fn=None):
    super(self.__class__, self).__init__(dataset,
        batch_size=batch_size,
        shuffle=shuffle,
        sampler=sampler,
        batch_sampler=batch_sampler,
        num_workers=num_workers,
        collate_fn=numpy_collate,
        pin_memory=pin_memory,
        drop_last=drop_last,
        timeout=timeout,
        worker_init_fn=worker_init_fn)

class FlattenAndCast(object):
  def __call__(self, pic):
    return np.array(Resize(size=[4,4])(pic), dtype=np.float32)
  
def data(batch_size,n_targets):
    # Define our dataset, using torch datasets
    # mnist_dataset = MNIST('/tmp/mnist/', download=True, transform=FlattenAndCast())
    mnist_dataset = MNIST('/tmp/mnist/', download=True, transform=FlattenAndCast())
    training_generator = NumpyLoader(mnist_dataset, batch_size=batch_size, num_workers=0)
    # Get the full train dataset (for checking accuracy while training)
    # train_images = jnp.array(Resize(size = [4,4])(mnist_dataset.train_data))
    train_images = np.array(Resize(size = [4,4])(mnist_dataset.train_data), dtype = np.float)
    # train_images = np.array(Resize(size = [8,8])(mnist_dataset.train_data[:600]))
    # train_labels = one_hot(np.array(mnist_dataset.train_labels[:train_images.shape[0]]), n_targets)
    train_labels = np.array(mnist_dataset.train_labels[:train_images.shape[0]])
    # Get full test dataset
    # mnist_dataset_test = MNIST('/tmp/mnist/', download=True, train=False)
    mnist_dataset_test = MNIST('/tmp/mnist/', download=True, train=False)
    # test_images = jnp.array(Resize(size = [8,8])(mnist_dataset_test.test_data[:100]))
    # test_images = jnp.array(Resize(size = [4,4])(mnist_dataset_test.test_data))
    test_images = np.array(Resize(size = [4,4])(mnist_dataset_test.test_data), dtype = np.float)
    # test_labels = one_hot(jnp.array(mnist_dataset_test.test_labels[:test_images.shape[0]]), n_targets)
    test_labels = np.array(mnist_dataset_test.test_labels[:test_images.shape[0]])

    return training_generator, train_images, train_labels, test_images, test_labels
  
def filter_(x, y, first, second):
    keep = (y == first) | (y == second)
    x, y = x[keep], y[keep]
    y = y == first
    return x, y
# class FlattenAndCast(object):
#   def __call__(self, pic):
#     return np.ravel(np.array(pic, dtype=jnp.float32))
  
# def data(batch_size, n_targets):
#     mnist_dataset = MNIST('/tmp/mnist/', download=True, transform=FlattenAndCast())
#     training_generator = NumpyLoader(mnist_dataset, batch_size=batch_size, num_workers=0)
#     # Get the full train dataset (for checking accuracy while training)
#     train_images = np.array(mnist_dataset.train_data).reshape(len(mnist_dataset.train_data), -1)
#     train_labels = one_hot(np.array(mnist_dataset.train_labels), n_targets)

#     # Get full test dataset
#     mnist_dataset_test = MNIST('/tmp/mnist/', download=True, train=False)
#     test_images = jnp.array(mnist_dataset_test.test_data.numpy().reshape(len(mnist_dataset_test.test_data), -1), dtype=jnp.float32)
#     test_labels = one_hot(np.array(mnist_dataset_test.test_labels), n_targets)
#     return training_generator, train_images, train_labels, test_images, test_labels