import numpy as np
import jax.numpy as jnp
from qhollab import*

def Сircuit(theta: list, sv, measure = False):
    theta = jnp.pi*10*theta
    # circ = Qft()
    circ = H[0]*H[0]
    circ *=  RotationX(theta[0])[0]*CNOT(0,2)*RotationX(theta[1])[1]*RotationX(theta[2])[2]*CNOT(1,3)*CNOT(2,1)*RotationX(theta[3])[3]*RotationX(theta[4])[1]
    len_sv = len(sv.data_shape)
    #print(len_sv)
    # for i in range(len_sv):
    #     circ *= SWAP[i, i+1]
    
    # layer = H[0]*H[1]*H[2]*H[3]*H[4]*H[5]*SWAP[5,4]*SWAP[3,4]*Swap()[2,3]*SWAP[2,1]*SWAP[0,1]*RotationX(theta[0])[0]*RotationX(theta[1])[1]*RotationX(theta[2])[2]*RotationX(theta[3])[3]*RotationX(theta[4])[4]*RotationX(theta[5])[5]
    # for i in range(1):
    #     circ *= layer
    if measure == True:
        for i in range(len_sv-1):
            circ *= Measurement(i)
    #print(circ)
    return circ

def Pooling_q(circ):
    '''
    For now we estimated only 2-by-2 kernel
    '''
    scheme_len = circ.estimated_len
    circ *= CNOT(2, 0)*CNOT(3, 0)*CNOT(3, 1)*CNOT(2, 1)
    circ *= Measurement(0)*Measurement(1)
    return circ

def Param_Shift(circ, theta, pic):
    #grad = (simulate(circ(theta+np.pi/2*np.ones(theta.shape)), pic).data-simulate(circ(theta+np.pi/2*np.ones(theta.shape)), pic).data)/2
    grad = circ(theta+np.pi/2*np.ones(theta.shape))(pic)-circ(theta+np.pi/2*np.ones(theta.shape))(pic)/2
    return grad